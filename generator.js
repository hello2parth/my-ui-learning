let x = 2;

function *foo() {
  console.log(x);
  yield;
  console.log(x);
}

const it = foo();
it.next();
x++;
console.log('x increased outside the function');
it.next();
