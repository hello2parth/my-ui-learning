// interview answer

const sMyName = 'Parth Patel';

const aMyName = [];

for (let i = 0; i < sMyName.length; i++) {
  aMyName.push(sMyName.charAt(i));
}
const result = [];
result.push(aMyName[0]);
aMyName.forEach(obj => {
  if (obj) {
    const flag = result.some(v => obj === v);
    if (!flag) {
      result.push(obj);
    }
  }
});
console.log(
  sMyName,
  result.reduce((accumlator, i) => accumlator + i)
);

// optimized answer after interview

function removeDuplicates(sourceString) {
  let result = '';
  for (let i = 0; i < sourceString.length; i++) {
    const ithCharacter = sourceString.charAt(i);
    if (result.indexOf(ithCharacter) === -1) {
      result += ithCharacter;
    }
  }
  return result;
}

console.log(removeDuplicates('Parth Patel'));
