/* eslint-disable prefer-rest-params */
const sum = (a, b, c) => a + b + c;
const memoize = cb => {
  const results = [];
  return function memoized() {
    for (let i = 0; i < results.length; i++) {
      const [currentArgs, currentResult] = results[i];
      // console.log({})
      let isArgumentsMatched = true;
      if (arguments.length === currentArgs.length) {
        for (let j = 0; j < arguments.length; j++) {
          if (arguments[j] !== currentArgs[j]) {
            isArgumentsMatched = false;
            break;
          }
        }
        if (isArgumentsMatched) {
          console.log('Cache', currentResult);
          return currentResult;
        }
      }
    }
    const result = cb(...arguments);
    console.log('Executed', result);
    results.push([arguments, result]);
    return result;
  };
};

const memoizedSum = memoize(sum);
memoizedSum(1, 2, 3);
memoizedSum(1, 2, 3);

memoizedSum(1, 2, '3');
memoizedSum(1, 2, '3');

const obj1 = { a: 123 };

memoizedSum(obj1, 2, 3);
memoizedSum(obj1, 2, 3);

memoizedSum({ a: 123 }, 2, 3);
memoizedSum({ a: 123 }, 2, 3);
