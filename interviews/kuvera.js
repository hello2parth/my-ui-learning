/* eslint-disable array-callback-return */
/* eslint-disable no-inner-declarations */

console.log('Problem: Sort on empty array');
try {
  const result = [].sort();
  console.log(result);
} catch (e) {
  console.log(e);
}

console.log('------------------------------------- \n');
console.log('Problem: sort on array of numbers');
try {
  const result = [-8, -2, 0, 8, 30].sort();
  console.log(result);
} catch (e) {
  console.log(e);
}
console.log(
  "Learning: It's not giving correct result because sort function first convert it into string and then filter"
);

console.log('------------------------------------- \n');
console.log('Solution: sort on array of numbers');
try {
  const result = [-8, -2, 0, 8, 30].sort((a, b) => {
    if (a > b) return 1;
    if (a < b) return -1;
    return 0; // a === b;
  });
  console.log(result);
} catch (e) {
  console.log(e);
}
console.log('------------------------------------- \n');
console.log('Reduce on empty array');
try {
  const result = [].reduce(() => {}); // if no call back function then throws error: undefined function
  console.log(result);
} catch (e) {
  console.log(e);
}
console.log("Learning: Trying to print first argument but it's empty error, so giving error of no initial value");
try {
  const result = [1, 2, 3].reduce(() => {});
  console.log(result);
} catch (e) {
  console.log(e);
}
console.log("Learning: Trying to print first argument but it's not passed, so giving error of undefined");
try {
  const result = [1, 2, 3].reduce(() => {});
  console.log(result);
} catch (e) {
  console.log(e);
}
console.log(
  'Learning: Trying to print first argument but there is no return in function block, so giving error of undefined'
);
try {
  const result = [1, 2, 3].reduce(accumalator => accumalator);
  console.log(result);
} catch (e) {
  console.log(e);
}
console.log('Learning: Printed first argument');
try {
  const result = [1, 2, 3].reduce((accumalator, currentValue) => accumalator + currentValue);
  console.log(result);
} catch (e) {
  console.log(e);
}
console.log('Learning: Printed sum');
console.log('------------------------------------- \n');
console.log('Calling function without accepting arguments to function defination');
try {
  function testFunction() {
    console.log(this.name);
  }
  testFunction.call({ name: 'Parth' });
} catch (e) {
  console.log(e);
}
console.log('------------------------------------- \n');
console.log('Problem of function constructor');
try {
  function TestFunction() {
    this.name = 'Parth';
    this.printMyCity = () => {
      console.log('My city name is Bangalore');
    };
  }
  const oFunction = new TestFunction();
  console.log(oFunction.name);
  console.log(oFunction.printMyCity());
  console.log(oFunction.b); // properties without this, can not be accessible
} catch (e) {
  console.log(e);
}
console.log('------------------------------------- \n');
console.log('Adding custom function to Array');
try {
  Array.prototype.getValues = function getValues() {
    return this; // this will pass the array
  };
  console.log([3, 4, 5].getValues());
} catch (e) {
  console.log(e);
}

console.log('------------------------------------- \n');
console.log('How to exceed call stack?');
try {
  function a() {
    a();
  } // Recursive calling
  a();
} catch (e) {
  console.log(e);
}
