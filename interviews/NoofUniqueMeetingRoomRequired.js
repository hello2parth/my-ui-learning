// Asked in interview of sg analytics
// Find No of unique rooms required to host the following oMeetings.
// Assume meeeting can be switched in zero time. i.e. a: [3, 3.30],  e: [3.30, 4] can be hosted in same meeting room

const oMeetings = [
  [3, 3.3],
  [3, 3.15],
  [3.15, 3.45],
  [4, 4.3],
  [3.3, 4],
  [3.45, 4.15],
  [3.45, 4],
  [3.3, 4.15],
];

oMeetings.sort((a, b) => a[0] - b[0]);

const aRooms = [];
for (let i = 0; i < oMeetings.length; i++) {
  const [meetingStartTime, meetingEndTime] = oMeetings[i];
  let isRoomAvailable = false;
  if (aRooms.length) {
    for (let j = 0; j < aRooms.length; j++) {
      if (meetingStartTime >= aRooms[j]) {
        isRoomAvailable = true;
        aRooms[j] = meetingEndTime;
      }
    }
  }
  if (!isRoomAvailable) {
    aRooms.push(meetingEndTime);
  }
}

console.log('Ending time of meeting in each room:', aRooms);
console.log('Total no. of room required to host meetings', aRooms.length);
