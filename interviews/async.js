/* Determine the output of following code */

try {
  setTimeout(() => {
    console.log(0);
  }, 200);

  console.log(1);

  setTimeout(() => {
    console.log(2);
  }, 100);

  new Promise(resolve => {
    console.log(3);
    resolve();
    console.log(4);
  }).then(() => {
    console.log(5);
  });

  console.log(6);

  async function test1() {
    console.log(7);
    await test2();
    console.log(8);
  }

  async function test2() {
    console.log(9);
  }
  new Promise(resolve => {
    console.log(10);
    resolve();
    console.log(11);
  }).then(() => {
    console.log(12);
  });

  await test1();

  console.log(13);
} catch (e) {
  console.log(e);
}

console.log(
  "Learning: First priority will be synchronous operation, second will be 'then' and third will be setTimeout"
);
