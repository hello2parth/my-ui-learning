for (var k = 0; k < 10; k++) {
  setTimeout(() => {
    console.log(k);
  }, 1000);
}

console.log('Passing correct scope to setTimeout');

for (let i = 0; i < 10; i++) {
  setTimeout(
    function () {
      console.log(this.i);
    }.bind({ i }),
    1000
  );
  // setTimeout(function(){console.log(this.j);}.bind({j:i}), 1000); initial implementation
}

/* correct scope can be passed by IIFE (immediately invoked function expression)
It's a one type of closure */

for (let j = 0; j < 10; j++) {
  (function IIFE(k) {
    setTimeout(() => {
      console.log(k);
    }, 1000);
  })(j);
}

console.log('Returning second highest number from array');
function secondHighest(array) {
  let highest = 0;
  let second_highest = 0;
  for (let i = 0; i < array.length; i++) {
    if (array[i] > highest) {
      second_highest = highest;
      highest = array[i];
    }
  }
  return second_highest;
}

console.log(secondHighest([3, 5, 7, 2, 8]));

const aTest = [3, 5, 7, 2, 8];
aTest.sort((a, b) => a - b);

console.log(aTest[aTest.length - 2]);
