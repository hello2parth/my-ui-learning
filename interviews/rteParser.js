// const _ = require('lodash');

const typeVsElement = {
  ordered: 'ol',
  bullet: 'ul',
};

const input = [
  { text: 'One', indent: 0, type: 'ordered' },
  { text: 'Two', indent: 0, type: 'ordered' },
  { text: 'Alpha', indent: 1, type: 'bullet' },
  { text: 'Beta', indent: 1, type: 'bullet' },
  { text: 'I', indent: 2, type: 'ordered' },
  { text: 'II', indent: 2, type: 'ordered' },
  { text: 'Three', indent: 0, type: 'ordered' },
];

function getParsedInput(input) {
  const result = {};
  if (Array.isArray(input)) {
    let lastIndent = '';
    const path = [];
    for (let i = 0; i < input.length; i++) {
      const { text, indent, type } = input[i];
      if (path.length === 0) {
        _.set(result, path, { [typeVsElement[type]]: [] });
        path.push(typeVsElement[type]);
      }
      if (indent > lastIndent) {
        const currentValue = _.get(result, path) || [];
        path.push(currentValue.length);
        _.set(result, path, { [typeVsElement[type]]: [] });
        path.push(typeVsElement[type]);
      }
      if (indent < lastIndent) {
        path.length = indent + 1;
      }
      const currentValue = _.get(result, path) || [];
      _.set(result, path, [...currentValue, { text }]);
      lastIndent = indent;
    }
  }
  return result;
}

console.log(getParsedInput(input, {}));

// let result = "";
// let idx = 0;

/*
<ol>
    <li>One</li>
    <li>Two</li>
    <ul>
       <li>Alpha</li>
       <li>Beta</li>
       <ol>
          <li>I</li>
          <li>II</li>
       </ol>
    </ul>
    <li>Three</li>
</ol>
*/
const deltaToHtml = parsedInput => {
  if (Array.isArray(parsedInput)) {
    let content = '';
    for (let i = 0; i < parsedInput.length; i++) {
      const item = parsedInput[i];
      // eslint-disable-next-line eqeqeq
      if (item.text == undefined) {
        content += deltaToHtml(item);
      } else {
        content += `<li>${item.text}</li>`;
      }
    }
    return content;
  }
  const keys = Object.keys(parsedInput);
  if (keys.length === 1) {
    const ele = keys[0];
    const content = parsedInput[ele];
    if (Array.isArray(content)) {
      return `<${ele}>${deltaToHtml(content)}</${ele}>`;
    }
  }
};

// console.log(deltaToHtml(parsedInput));
