const array = [
  { name: 'A', age: 21 },
  { name: 'B', age: 22 },
  { name: 'C', age: 23 },
];
// Print  A:21 B:22 C:23

const result = array.reduce((acc, item) => {
  const { name, age } = item;
  return `${acc} ${name}:${age}`;
}, '');

console.log(result);

const fruits = ['Banana', 'Orange', 'Apple', 'Orange', 'Pear', 'Banana'];
/* Find fruit count and print
Apple: 1,
Banana: 2,
Orange: 2,
Pear: 1 */

const fruitCount = fruits.reduce((acc, fruit) => {
  if (acc[fruit]) {
    acc[fruit] += 1;
  } else {
    acc[fruit] = 1;
  }
  return acc;
}, {});

console.log({ fruitCount });

// https://codepen.io/parth55/pen/JjBQQWp?editors=1010

const generatePromise = (name, baseValue) =>
  new Promise(res => {
    setTimeout(() => {
      res(`${name} ${baseValue || ''}`);
      console.log(`${name} ${baseValue || ''} ${Date.now()}`);
    }, 3000);
  });

const arr = [
  generatePromise.bind(null, 'Apple'),
  generatePromise.bind(null, 'Ball'),
  generatePromise.bind(null, 'Cat'),
];

arr.reduce((acc, val) => acc.then(res => val(res)), Promise.resolve(''));
