function click() {
  let time1;
  let time2;
  function trippleClick() {
    if (!time1) {
      time1 = +new Date();
      return;
    }

    if (time1 && !time2) {
      const currentTime = +new Date();
      if (currentTime > time1 + 100) {
        time1 = null;
        time2 = null;
      } else {
        time2 = currentTime;
      }
      return;
    }

    if (time1 && time2) {
      time1 = null;
      time2 = null;
      console.log('tripple click is done');
    }
  }
  return trippleClick;
}
const trippleClick = click();

trippleClick();
setTimeout(() => {
  trippleClick();
  trippleClick();
}, 50);
