/* eslint-disable no-unused-vars */
/* eslint-disable no-prototype-builtins */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */

const input1 = {
  a: 'a1',
  b: 'b2',
  g: {
    a: 'q',
    c: {
      c1: 'c1',
      c2: 'c2',
    },
  },
  d: 'd1',
  f: 'f1',
};

const input2 = {
  a: 'a1',
  g: {
    a: 'p',
    c: {
      c1: 'c1',
      c2: 'c2',
      c3: 'c3',
    },
  },
  d: 'd2',
};

const output = {
  b: 'b2',
  g: {
    a: 'q',
    c: {
      c3: 'c3',
    },
  },
  d: 'd1',
  f: 'f1',
};

function diff(o1, o2) {
  const result = {};
  for (const key in o1) {
    if (typeof o1[key] === 'object') {
      result[key] = diff(o1[key], o2[key]);
    } else if (o1[key] !== o2[key]) {
      result[key] = o1[key];
    }
  }
  for (const key in o2) {
    if (typeof o2[key] === 'object') {
      result[key] = diff(o1[key], o2[key]);
    } else if (o2[key] && !o1[key]) {
      result[key] = o2[key];
    }
  }
  return result;
}

console.log(diff(input1, input2));
// asked in toppr second round
