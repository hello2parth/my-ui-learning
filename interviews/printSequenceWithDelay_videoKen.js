// Print sequence of number untill end with specified delay and step size.
// 100 delay (2sec) stepSize(2) print 102 delay (2sec) stepSize(2) print 104 ...till 120

function print(n) {
  setTimeout(() => {
    console.log(n);
    if (n > 0) {
      print(n - 1);
    }
  }, 1000);
}
print(10);

function printUsingInterval(n) {
  let i = 0;
  const interval = setInterval(() => {
    console.log(i);
    i++;
    if (i > n) {
      clearInterval(interval);
    }
  }, 1000);
}
printUsingInterval(10);

function printUsingLoop(n) {
  for (let i = 0; i <= n; i++) {
    setTimeout(() => {
      console.log(i);
    }, i * 1000);
  }
}

printUsingLoop(10);

async function printUsingLoopAndPromise(n) {
  for (let i = 0; i <= n; i++) {
    await new Promise(resolve => {
      setTimeout(() => {
        resolve(console.log(i));
      }, 1000);
    });
  }
}

printUsingLoopAndPromise(10);
