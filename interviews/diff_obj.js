/* eslint-disable no-prototype-builtins */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
const input1 = {
  a: 'a1',
  b: 'b2',
  g: {
    a: 'q',
    c: {
      c1: 'c1',
      c2: 'c2',
    },
  },
  d: 'd1',
  f: 'f1',
};

const input2 = {
  a: 'a1',
  g: {
    a: 'p',
    c: {
      c1: 'c1',
      c2: 'c2',
      c3: 'c3',
    },
  },
  d: 'd2',
};

function diff(obj1, obj2) {
  const result = {};
  for (const key1 in obj1) {
    const obj2hasKey1 = obj2.hasOwnProperty(key1);
    const obj1key1Type = typeof obj1[key1];
    if (obj2hasKey1) {
      if (typeof obj2[key1] === obj1key1Type && obj1[key1] !== obj2[key1]) {
        if (obj1key1Type === 'object') {
          const abc = diff(obj1[key1], obj2[key1]);
          if (Object.keys(abc).length > 0) {
            result[key1] = abc;
          }
        } else {
          result[key1] = obj1[key1];
        }
      }
    } else {
      result[key1] = obj1[key1];
    }
  }
  return result;
}

console.log(diff(input2, input1));
