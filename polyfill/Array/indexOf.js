if (!Array.prototype.myIndexOf) {
  Array.prototype.myIndexOf = function myIndexOf(element, fromIndex = 0) {
    let start = fromIndex;
    const { length } = this;
    if (start < 0) {
      start = start + fromIndex >= 0 ? start + fromIndex : 0;
    } else if (start > length) {
      start = length;
    }
    for (let i = start; i < length; i++) {
      if (this[i] === element) {
        return i;
      }
    }
    return -1;
  };
}

const aTest = [1, 2, 3, 2];
console.log(aTest.indexOf(2, 2));
console.log(aTest.myIndexOf(2, 2));
