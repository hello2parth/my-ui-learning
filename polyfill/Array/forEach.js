const aTest = [1, 2, 3, 4, 5, 6, 7, 8, 9, null];

aTest.forEach(
  function Iterator(v, i) {
    console.log(v, i, this.name);
  },
  { name: 'Parth' }
);

if (!Array.prototype.myForEach) {
  Array.prototype.myForEach = function myForEach(cb, context) {
    for (let i = 0; i < this.length; i++) {
      cb.call(context, this[i], i, this);
    }
  };
}

aTest.myForEach(
  function Iterator(v, i) {
    console.log(v, i, this.name);
  },
  { name: 'Parth' }
);
