if (!Array.prototype.myUnshift) {
  Array.prototype.myUnshift = function myUnshift(...items) {
    const array = [...this];
    this.length = 0;
    for (let i = 0; i < items.length; i++) {
      this[i] = items[i];
    }
    for (let i = 0; i < array.length; i++) {
      this[i + items.length] = array[i];
    }
    return this.length;
  };
}

const aTest = [4, 5, 6];
console.log(aTest.unshift(1, 2), aTest);

const aTest1 = [4, 5, 6];
console.log(aTest1.myUnshift(1, 2), aTest1);
