if (!Array.prototype.mySome) {
  Array.prototype.mySome = function mySome(cb, context) {
    for (let i = 0; i < this.length; i++) {
      if (cb.call(context, this[i], i, this)) {
        return true;
      }
    }
    return false;
  };
}

const aTest = [1, 2, 'Parth', 4];

console.log(aTest.some(v => typeof v === 'string'));
console.log(aTest.mySome(v => typeof v === 'string'));

console.log(aTest.some(v => typeof v === 'boolean'));
console.log(aTest.mySome(v => typeof v === 'boolean'));
