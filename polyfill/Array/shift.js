if (!Array.prototype.myShift) {
  Array.prototype.myShift = function myShift() {
    let result;
    const temp = [...this];
    this.length = 0;
    if (temp.length > 0) {
      [result] = temp;
      for (let i = 1; i < temp.length; i++) {
        this.push(temp[i]);
      }
    }
    return result;
  };
}

const aTest = [1, 2, 3];
console.log(aTest.shift(), aTest);
console.log(aTest.shift(), aTest);
console.log(aTest.shift(), aTest);
console.log(aTest.shift(), aTest);

const aTest1 = [1, 2, 3];
console.log(aTest1.myShift(), aTest1);
console.log(aTest1.myShift(), aTest1);
console.log(aTest1.myShift(), aTest1);
console.log(aTest1.myShift(), aTest1);
