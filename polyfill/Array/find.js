if (!Array.prototype.myFind) {
  Array.prototype.myFind = function myFind(cb, context) {
    for (let i = 0; i < this.length; i++) {
      if (cb.call(context, this[i], i, this)) {
        return this[i];
      }
    }
    return undefined;
  };
}

const aTest = [1, 2, 3, 4, 5];
console.log(aTest.find(v => v > 2));
console.log(aTest.myFind(v => v > 2));
