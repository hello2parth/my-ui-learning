if (!Array.prototype.myFindLastIndex) {
  Array.prototype.myFindLastIndex = function myFindLastIndex(cb, context) {
    for (let i = this.length; i >= 0; i--) {
      if (cb.call(context, this[i], i, this)) {
        return i;
      }
    }
    return -1;
  };
}

const aTest = [1, 2, 3, 4, 5];
console.log(aTest.findLastIndex(v => v > 3));
console.log(aTest.myFindLastIndex(v => v > 3));
