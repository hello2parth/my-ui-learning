if (!Array.prototype.myEntries) {
  Array.prototype.myEntries = function myEntries() {
    function* ArrayIterator() {
      for (let i = 0; i < this.length; i++) {
        yield [i, this[i]];
      }
    }
    return ArrayIterator.call(this);
  };
}

const a = [1, 2, 3];
const arrayIterator = a.myEntries();
console.log(arrayIterator);
console.log(arrayIterator.next());
console.log(arrayIterator.next());
console.log(arrayIterator.next());
console.log(arrayIterator.next());
