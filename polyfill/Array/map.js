const aTest = [3, 4, 5];
console.log('original array', aTest);
const aResult = aTest.map(
  function mapper(v) {
    return v + this.addvalue;
  },
  { addvalue: 5 }
);

console.log('mapped array', aResult);

if (!Array.prototype.myMap) {
  Array.prototype.myMap = function myMap(cb, context) {
    const result = [];
    for (let i = 0; i < this.length; i++) {
      result.push(cb.call(context, this[i], i, this));
    }
    return result;
  };
}

const aPolyfillResult = aTest.myMap(
  function mapper(v) {
    return v + this.addvalue;
  },
  { addvalue: 5 }
);
console.log('map polyfill result', aPolyfillResult);
