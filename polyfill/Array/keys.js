if (!Array.prototype.myKeys) {
  Array.prototype.myKeys = function myKeys() {
    function* Iterator() {
      for (let i = 0; i < this.length; i++) {
        yield i;
      }
    }
    return Iterator.call(this);
  };
}

const aTest = [1, 2, 3];
const keysOfaTest = aTest.myKeys();
console.log(keysOfaTest.next());
console.log(keysOfaTest.next());
console.log(keysOfaTest.next());
console.log(keysOfaTest.next());
