if (!Array.prototype.myAt) {
  Array.prototype.myAt = function myAt(index = 0) {
    let postion = index;
    if (postion < 0) {
      postion = postion + this.length > 0 ? postion + this.length : 0;
    }
    return this[postion];
  };
}

const test = [1, 2, 3, 4];
const test1 = [];
test1[-2] = 5;

console.log(test.at(2), test.myAt(2));
console.log(test.at(-1), test.myAt(-1));
console.log(test1.at(-1), test1.myAt(-1));
console.log(test1.at(), test1.myAt());
