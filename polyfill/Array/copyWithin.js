if (!Array.prototype.myCopyWithin) {
  Array.prototype.myCopyWithin = function myCopyWithin(index, start = 0, end = this.length) {
    const originalArray = [...this];
    let _index = index > this.length ? this.length : index;
    if (_index < 0) {
      _index = _index + this.length > 0 ? _index + this.length : 0;
    }
    let _start = start > this.length ? this.length : start;
    if (_start < 0) {
      _start = _start + this.length > 0 ? _start + this.length : 0;
    }
    let _end = end > this.length ? this.length : end;
    if (_end < 0) {
      _end = _end + this.length > 0 ? _end + this.length : 0;
    }
    for (let i = _start; i < _end && _index <= _end; i++, _index++) {
      this[_index] = originalArray[i];
    }
    return this;
  };
}
console.log([1, 2, 3, 4, 5, 6].copyWithin(1, 2));
console.log([1, 2, 3, 4, 5, 6].myCopyWithin(1, 2));

console.log([1, 2, 3, 4, 5, 6].copyWithin(-2, -3, -1));
console.log([1, 2, 3, 4, 5, 6].myCopyWithin(-2, -3, -1));

console.log([1, 2, 3, 4, 5, 6].copyWithin(-18, -3, -1));
console.log([1, 2, 3, 4, 5, 6].myCopyWithin(-18, -3, -1));
