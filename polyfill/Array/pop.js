if (!Array.prototype.myPop) {
  Array.prototype.myPop = function myPop() {
    const result = this[this.length - 1];
    if (this.length > 0) {
      this.length -= 1;
    }
    return result;
  };
}

const aTest = [1, 2, 3];
console.log(aTest.pop(), aTest);
console.log(aTest.pop(), aTest);
console.log(aTest.pop(), aTest);
console.log(aTest.pop(), aTest);

const aTest1 = [1, 2, 3];
console.log(aTest1.myPop(), aTest1);
console.log(aTest1.myPop(), aTest1);
console.log(aTest1.myPop(), aTest1);
console.log(aTest1.myPop(), aTest1);
