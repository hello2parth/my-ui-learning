if (!Array.prototype.myFlatMap) {
  Array.prototype.myFlatMap = function myFlatMap(cb, context) {
    let result = [];
    for (let i = 0; i < this.length; i++) {
      result = result.concat(cb.call(context, this[i], i, this));
    }
    return result;
  };
}

const aTest = [1, 2, 3, 4];

console.log(aTest.flatMap(x => [[x * 2]]));
console.log(aTest.myFlatMap(x => [[x * 2]]));
