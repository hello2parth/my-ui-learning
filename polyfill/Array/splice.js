Array.prototype.mySplice = function mySplice(start = 0, deleteCount = 0, ...Items) {
  const clonedArray = [...this];
  const deletedArray = [];
  const clonedArrayLength = clonedArray.length;

  let _start = start;

  if (start > clonedArrayLength) {
    _start = clonedArrayLength;
  } else if (start < 0) {
    _start = clonedArrayLength + start > 0 ? clonedArrayLength + start : 0;
  }

  const _deleteCount =
    // eslint-disable-next-line no-nested-ternary
    deleteCount + _start >= clonedArrayLength ? clonedArrayLength : deleteCount < 0 ? 0 : deleteCount;

  this.length = _start > clonedArrayLength ? clonedArrayLength : _start; // till the index, no change

  const loopLength = _start + _deleteCount > clonedArrayLength ? clonedArrayLength : _start + _deleteCount;
  for (let i = _start; i < loopLength; i++) {
    // index to length, delete the items
    deletedArray.push(clonedArray[i]);
  }
  // push the new items
  this.push(...Items);
  for (let i = _start + _deleteCount; i < clonedArrayLength; i++) {
    // add remaining items from index+length to array.length
    this.push(clonedArray[i]);
  }
  return deletedArray;
};

let test = [100, 200, 300, 400];
let result = test.mySplice(-1, 10, 15, 20);
console.log(test, result);

let test1 = [100, 200, 300, 400];
let result1 = test1.splice(-1, 10, 15, 20);
console.log(test1, result1);

test = [100, 200, 300, 400];
result = test.mySplice(2, 1, 15, 20);
console.log(test, result);

test1 = [100, 200, 300, 400];
result1 = test1.splice(2, 1, 15, 20);
console.log(test1, result1);

test = [100, 200, 300, 400];
result = test.mySplice(undefined, undefined, undefined);
console.log(test, result);

test1 = [100, 200, 300, 400];
result1 = test1.splice(undefined, undefined, undefined);
console.log(test1, result1);

test = [100, 200, 300, 400];
result = test.mySplice(Infinity, Infinity, Infinity);
console.log(test, result);

test1 = [100, 200, 300, 400];
result1 = test1.splice(Infinity, Infinity, Infinity);
console.log(test1, result1);

test = [100, 200, 300, 400];
result = test.mySplice(1, undefined, 2);
console.log(test, result);

test1 = [100, 200, 300, 400];
result1 = test1.splice(1, undefined, 2);
console.log(test1, result1);

test = [100, 200, 300, 400];
result = test.mySplice(1);
console.log(test, result);

test1 = [100, 200, 300, 400];
result1 = test1.splice(1);
console.log(test1, result1);

// Splice charactristics
// 1. if no arguments passed in spice then original array will remain unchanged and returning array is null
// 2. first argumeny index should be between 0 to array.length
// 3. second argument (length) should be between 0 to length of array
