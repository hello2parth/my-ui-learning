if (!Array.prototype.myConcat) {
  Array.prototype.myConcat = function myConcat(...items) {
    const result = [...this];
    for (let i = 0; i < items.length; i++) {
      const item = items[i];
      if (Array.isArray(item)) {
        result.push(...item);
      } else {
        result.push(item);
      }
    }
    return result;
  };
}

console.log([1, 2, 3].concat(4, 5, 6), [1, 2, 3].myConcat(4, 5, 6));
console.log([1, 2, 3].concat([4, 5, 6]), [1, 2, 3].myConcat([4, 5, 6]));
console.log([1, 2, 3].concat([4], 5, 6), [1, 2, 3].myConcat([4], 5, 6));
console.log([1, 2, 3].concat(null), [1, 2, 3].myConcat(null));
