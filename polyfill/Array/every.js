if (!Array.prototype.myEvery) {
  Array.prototype.myEvery = function myEvery(cb, context) {
    for (let i = 0; i < this.length; i++) {
      if (!cb.call(context, this[i], i, this)) {
        return false;
      }
    }
    return true;
  };
}

console.log([1, 2, 3].every(Number.isFinite));
console.log([1, 2, 3].myEvery(Number.isFinite));

console.log([1, 2.1, 3].every(Number.isInteger));
console.log([1, 2.2, 3].myEvery(Number.isInteger));
