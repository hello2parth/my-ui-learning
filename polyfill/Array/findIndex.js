if (!Array.prototype.myFindIndex) {
  Array.prototype.myFindIndex = function myFindIndex(cb, context) {
    for (let i = 0; i < this.length; i++) {
      if (cb.call(context, this[i], i, this)) {
        return i;
      }
    }
    return -1;
  };
}

const aTest = [1, 2, 3, 4, 5];
console.log(aTest.findIndex(v => v > 2));
console.log(aTest.myFindIndex(v => v > 2));
console.log(aTest.findIndex(v => v > 10));
console.log(aTest.myFindIndex(v => v > 10));
