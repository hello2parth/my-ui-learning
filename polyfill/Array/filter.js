if (!Array.prototype.myFilter) {
  Array.prototype.myFilter = function myFilter(cb, context) {
    const result = [];
    for (let i = 0; i < this.length; i++) {
      if (cb.call(context, this[i], i, this)) {
        result.push(this[i]);
      }
    }
    return result;
  };
}

if (!Array.prototype.myFilterUsingReduce) {
  Array.prototype.myFilterUsingReduce = function myFilterUsingReduce(cb, context) {
    return this.reduce(
      (acc, value, index, array) => (cb.call(context, value, index, array) ? acc.concat(value) : acc),
      []
    );
  };
}

const aTest = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
console.log(aTest.filter(v => v % 2 === 0));
console.log(aTest.myFilter(v => v % 2 === 0));
console.log(aTest.myFilterUsingReduce(v => v % 2 === 0));
console.log(aTest.filter(v => v > 5));
console.log(aTest.myFilter(v => v > 5));
console.log(aTest.myFilterUsingReduce(v => v > 5));
