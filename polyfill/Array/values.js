if (!Array.prototype.myValues) {
  Array.prototype.myValues = function myValues() {
    function* Iterator() {
      for (let i = 0; i < this.length; i++) {
        yield this[i];
      }
    }
    return Iterator.call(this);
  };
}

const aTest = [1, 2, 3];

console.log(Array.from(aTest.values()));
console.log(Array.from(aTest.myValues()));
