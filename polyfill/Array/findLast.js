if (!Array.prototype.myFindLast) {
  Array.prototype.myFindLast = function myFindLast(cb, context) {
    for (let i = this.length - 1; i >= 0; i--) {
      if (cb.call(context, this[i], i, this)) {
        return this[i];
      }
    }
    return undefined;
  };
}

const aTest = [1, 2, 3, 4, 5];
console.log(aTest.findLast(v => v > 3));
console.log(aTest.myFindLast(v => v > 3));
