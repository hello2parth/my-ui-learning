if (!Array.prototype.mySort) {
  Array.prototype.mySort = function mySort(compareFun) {
    function mergeSort(array) {
      if (array.length > 1) {
        const mid = Math.floor(array.length / 2);
        const left = array.slice(0, mid);
        const right = array.slice(mid, array.length);
        return merge(mergeSort(left), mergeSort(right));
      }
      return array;
    }

    function merge(left, right) {
      const result = [];
      while (left.length && right.length) {
        if (compareFun(left[0], right[0]) > 0) {
          result.push(right.shift());
        } else {
          result.push(left.shift());
        }
      }
      return result.concat(right, left);
    }

    return mergeSort(this);
  };
}

const aTest = [10, 8, 18, 15];
console.log(aTest.sort((a, b) => a - b));

const aTest1 = [10, 8, 18, 15];
console.log(aTest1.mySort((a, b) => a - b));
