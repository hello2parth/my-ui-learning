if (!Array.prototype.myIncludes) {
  Array.prototype.myIncludes = function myIncludes(element, fromIndex = 0) {
    let start = fromIndex;
    const arraySize = this.length;
    if (start > arraySize) {
      start = arraySize;
    } else if (start < 0) {
      start = start + arraySize >= 0 ? start + arraySize : 0;
    }

    for (let i = start; i < arraySize; i++) {
      if (this[i] === element) {
        return true;
      }
    }
    return false;
  };
}

const aTest = [1, 2, 3];
console.log(aTest.includes(2));
console.log(aTest.myIncludes(2));
console.log(aTest.includes('2'));
console.log(aTest.myIncludes('2'));
console.log(aTest.includes(2, 2));
console.log(aTest.myIncludes(2, 2));
