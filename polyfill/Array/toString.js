if (!Array.prototype.myToString) {
  Array.prototype.myToString = function myToString() {
    return this.join();
  };
}

const aTest = [1, 2, 3];
console.log(aTest.toString());
console.log(aTest.myToString());
