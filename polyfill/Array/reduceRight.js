/* eslint-disable array-callback-return */
if (!Array.prototype.myReduceRight) {
  Array.prototype.myReduceRight = function myReduceRight(cb, initialValue, context) {
    let start = this.length - 1;
    let accumlator = initialValue;
    if (!initialValue) {
      accumlator = this[start];
      start -= 1;
    }
    for (let i = start; i >= 0; i--) {
      accumlator = cb.call(context, accumlator, this[i], i, this);
    }
    return accumlator;
  };
}

const aTest1 = [1, 2, 3, 4, 5, 6, 7, 8, 9];
const result = aTest1.reduceRight((acc, val) => acc + val, 100);
const polyfillResult = aTest1.myReduceRight((acc, val) => acc + val, 100);

console.log(result);
console.log(polyfillResult);

const aTest2 = ['p', 'a', 'r', 't', 'h'];
console.log(aTest2.reduceRight((acc, val) => acc + val, 'patel'));
console.log(aTest2.myReduceRight((acc, val) => acc + val, 'patel'));

console.log([1, 2].reduceRight(() => {}));
console.log([1, 2].myReduceRight(() => {}));
