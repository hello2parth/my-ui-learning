function getFormmatedPosition(position, arrayLength) {
  let _position = position > arrayLength ? arrayLength : position;
  if (position < 0) {
    if (position + arrayLength >= 0) {
      _position = position + arrayLength;
    } else {
      _position = 0;
    }
  }
  return _position;
}

Array.prototype.mySlice = function slice(start = 0, end = this.length) {
  const result = [];
  const _start = getFormmatedPosition(start, this.length);
  const _end = getFormmatedPosition(end, this.length);
  for (let i = _start; i < _end; i++) {
    result.push(this[i]);
  }
  return result;
};

const testArray = [1, 2, 3, 4, 5, 6];
console.log(testArray.mySlice(2, 4), testArray.slice(2, 4));
console.log(testArray.mySlice(), testArray.slice());
console.log(testArray.mySlice(-Infinity, Infinity), testArray.slice(-Infinity, Infinity));
console.log(testArray.mySlice(-4, -2), testArray.slice(-4, -2));
