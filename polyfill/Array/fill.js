function getFormmatedPosition(start, end, arrayLength) {
  let _start = start > arrayLength ? arrayLength : start;
  if (_start < 0) {
    _start = _start + arrayLength > 0 ? _start + arrayLength : 0;
  }
  let _end = end > arrayLength ? arrayLength : end;
  if (_end < 0) {
    _end = _end + arrayLength > 0 ? _end + arrayLength : 0;
  }
  return { _start, _end };
}

if (!Array.myFill) {
  Array.prototype.myFill = function myFill(value, start = 0, end = this.length) {
    const { _start, _end } = getFormmatedPosition(start, end, this.length);
    for (let i = _start; i < _end; i++) {
      this[i] = value;
    }
    return this;
  };
}

console.log(Array(10).myFill(5), Array(10).fill(5));
console.log(Array(10).myFill(5, 2, 4), Array(10).fill(5, 2, 4));
console.log(Array(10).myFill(5, -4, -2), Array(10).fill(5, -4, -2));
