if (!Array.prototype.myReduce) {
  Array.prototype.myReduce = function myReduce(cb, initialValue, context) {
    let accumlator = initialValue;

    if (typeof initialValue === 'undefined') {
      const typeofSource = typeof this[0];
      if (typeofSource === 'number') {
        accumlator = 0;
      } else if (typeofSource === 'string') {
        accumlator = '';
      }
    }

    for (let i = 0; i < this.length; i++) {
      accumlator = cb.call(context, accumlator, this[i], i, this);
    }

    return accumlator;
  };
}
const aTest1 = [1, 2, 3, 4, 5, 6, 7, 8, 9];
const result = aTest1.reduce((acc, val) => acc + val, 100);
const polyfillResult = aTest1.myReduce((acc, val) => acc + val, 100);

console.log(result);
console.log(polyfillResult);

const aTest2 = ['p', 'a', 'r', 't', 'h'];
console.log(aTest2.reduce((acc, val) => acc + val, 'patel'));
console.log(aTest2.myReduce((acc, val) => acc + val, 'patel'));

// eslint-disable-next-line array-callback-return
console.log([1, 2].reduce(() => {}));
console.log([1, 2].myReduce(() => {}));
