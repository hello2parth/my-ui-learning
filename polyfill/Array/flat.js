const aTest = [
  [1],
  [2],
  [
    [1, 4, [5, 3]],
    [1, 2, 3, [3, 4, [2, [22, [3, 4, 5, 6, 5, [2]]]]], 4],
  ],
  { a: 5 },
];
// console.log(aTest);
// console.log(aTest.flat());

if (!Array.prototype.myFlat) {
  Array.prototype.myFlat = function myFlat(isRecurssive = false, deep = 0, aResult = []) {
    let _result = aResult;
    for (let i = 0; i < this.length; i++) {
      if (Array.isArray(this[i])) {
        if (isRecurssive || deep > 0) {
          _result = myFlat.call(this[i], isRecurssive, deep - 1, _result);
        } else {
          for (let j = 0; j < this[i].length; j++) {
            // flat only one array inside main array
            _result.push(this[i][j]);
          }
        }
      } else {
        _result.push(this[i]);
      }
    }
    return _result;
  };
}

if (!Array.prototype.myFlatUsingConcat) {
  Array.prototype.myFlatUsingConcat = function myFlatUsingConcat(isRecurssive, deep = 0) {
    return this.reduce((results, value) => {
      if (Array.isArray(value) && (isRecurssive || deep > 0)) {
        return results.concat(myFlatUsingConcat.call(value, isRecurssive, deep - 1));
      }
      return results.concat(value);
    }, []);
  };
}

console.log(aTest.myFlat());
console.log(aTest.myFlatUsingConcat());
console.log(aTest.myFlat(true));
console.log(aTest.myFlatUsingConcat(true));
console.log(aTest.myFlat(false, 7));
console.log(aTest.myFlatUsingConcat(false, 7));

// write a polyfill for isEqual
