if (!Array.prototype.myReverse) {
  Array.prototype.myReverse = function myReverse() {
    const result = [];
    while (this.length !== 0) {
      result.push(this.pop());
    }
    return result;
  };
}

const aTest = [1, 2, 3, 4];
console.log(aTest.reverse());

const aTest1 = [1, 2, 3, 4];
console.log(aTest1.myReverse());
