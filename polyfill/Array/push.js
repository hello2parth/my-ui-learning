if (!Array.prototype.push) {
  Array.prototype.myPush = function myPush(...items) {
    const { length } = this;
    const newLength = length + items.length;
    for (let i = length; i < newLength; i++) {
      this[i] = items[i];
    }
    return this.length;
  };
}

const aTest = [1, 2, 3];
console.log(aTest.push(8, 9), aTest);

const aTest1 = [1, 2, 3];
console.log(aTest1.push(8, 9), aTest1);
