if (!Array.prototype.myJoin) {
  Array.prototype.myJoin = function myJoin(separator = ',') {
    let result = '';
    for (let i = 0; i < this.length; i++) {
      if (this[i] !== undefined && this[i] !== null) {
        if (i === this.length - 1) {
          result += this[i];
        } else {
          result = result + this[i] + separator;
        }
      } else {
        result += separator;
      }
    }
    return result;
  };
}

// eslint-disable-next-line no-sparse-arrays
const aTest = [, 0, 2, 3, false, {}];
console.log(aTest.join('-'));
console.log(aTest.myJoin('-'));
