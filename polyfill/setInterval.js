const id = {};

function setIntervalPolyfill(cb, time) {
  cb();
  id[cb.toString()] = setTimeout(() => {
    setIntervalPolyfill(cb, time);
  }, time);
  return id[cb.toString()];
}

function clearIntervalPolyfill(id) {
  clearTimeout(id);
}

setIntervalPolyfill(() => console.log('Good morning, Parth!'), 1000);
const afternoonInterval = setIntervalPolyfill(() => console.log('Good afternoon, Parth!'), 1000);
setIntervalPolyfill(() => console.log('Good eveining, Parth!'), 1000);
clearIntervalPolyfill(afternoonInterval);
