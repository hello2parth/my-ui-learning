const window = {};
let queue = [];
let count = 0;

Object.defineProperty(window, 'myCustomTimeout', {
  value: (fn, time) => {
    count += 1;
    queue.push({ fn, time, timeStamp: Date.now(), count });
    runner();
    return count;
  },
});

Object.defineProperty(window, 'clearMyCustomTimeout', {
  value: id => {
    queue = queue.filter(({ count: _count }) => _count !== id);
  },
});

function runner() {
  const { executionQueue, updatedQueue } = queue.reduce(
    (acc, value) => {
      const { fn: _fn, time: _time, timeStamp } = value;
      if (Date.now() >= _time + timeStamp) {
        acc.executionQueue.push(_fn);
      } else {
        acc.updatedQueue.push(value);
      }
      return acc;
    },
    { executionQueue: [], updatedQueue: [] }
  );
  if (executionQueue.length) {
    requestAnimationFrame(() => {
      executionQueue.forEach(cb => cb());
    });
  }
  queue = updatedQueue;
  if (queue.length) {
    requestAnimationFrame(() => {
      requestAnimationFrame(runner);
    });
  }
}

const timeoutId = window.myCustomTimeout(() => alert('1'), 2000);
window.myCustomTimeout(() => {
  window.myCustomTimeout(() => alert('3'), 5000);
  alert('2');
}, 1000);
window.clearMyCustomTimeout(timeoutId);
