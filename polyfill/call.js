function foo() {
  console.log(this);
}

foo.call([1, 2, 3]);

Function.prototype.callPolyfill = function (context, ...args) {
  // Need fix
  // let func = this.bind(context); using bind
  // func(...args);
  if (typeof context === 'object') {
    // without using bind
    context.func = this;
    return context.func(...args);
  }
  const func = this;
  const obj = { func };
  obj.context = context;
  return obj.func(...args);
};

foo.callPolyfill([1, 2, 3]);

// Need Fix
// 1. this attachs the function defination
