const myDetails = {
  name: 'Parth',
  city: 'Lunawada',
};
const familyDetails = {
  occupation: 'teacher',
  siblings: 2,
};

function giveIntroduction(obj) {
  console.log(
    `My name is ${this.name} and I am from ${this.city}. My Parents are ${obj.occupation} and I have ${obj.siblings} siblings`
  );
}

const introduction = giveIntroduction.bind(myDetails);
introduction(familyDetails);

Function.prototype.bindPolyfill = function bindPolyfill() {
  const context = this;
  const outerArgs = arguments;
  return function () {
    const innerArgs = arguments;
    context.call(...outerArgs, ...innerArgs);
  };
};
const introPolyfill = giveIntroduction.bindPolyfill(myDetails);
introPolyfill(familyDetails);

// need further improvement to accept this at line #26 with call and apply
