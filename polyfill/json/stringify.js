/* eslint-disable no-prototype-builtins */
/* eslint-disable no-restricted-syntax */
if (!JSON.myStringify) {
  JSON.myStringify = function myStringify(data) {
    if (data === null) {
      return 'null';
    }
    if (Object.is(data, NaN)) {
      return 'null';
    }
    if (data === undefined) {
      return undefined;
    }
    if (typeof data === 'string') {
      return `"${data}"`;
    }
    if (typeof data === 'number') {
      return `${data}`;
    }
    if (typeof data === 'boolean') {
      return `${data}`;
    }
    if (Array.isArray(data)) {
      return `[${data.reduce((acc, item) => {
        const stringifiedItem = typeof item === 'undefined' ? null : JSON.myStringify(item);
        return acc ? `${acc},${stringifiedItem}` : stringifiedItem;
      }, '')}]`;
    }
    if (typeof data === 'object') {
      let result = '';
      for (const key in data) {
        if (data.hasOwnProperty(key)) {
          if (typeof data[key] !== 'undefined') {
            const currentString = `${JSON.myStringify(key)}:${JSON.myStringify(data[key])}`;
            result = result ? `${result},${currentString}` : result + currentString;
          }
        }
      }
      return `{${result}}`;
    }
    return undefined;
  };
}

console.log(JSON.stringify(null), JSON.myStringify(null));
console.log(JSON.stringify(undefined), JSON.myStringify(undefined));
console.log(JSON.stringify(NaN), JSON.myStringify(NaN));
console.log(JSON.stringify('a'), JSON.myStringify('a'));
console.log(JSON.stringify(5), JSON.myStringify(5));
console.log(JSON.stringify(true), JSON.myStringify(true));
console.log(
  JSON.stringify(() => {}),
  JSON.myStringify(() => {})
);
console.log(JSON.stringify([1, 2, 3, [], null, undefined, NaN]), JSON.myStringify([1, 2, 3, [], null, undefined, NaN]));
console.log(
  JSON.stringify({ a: 1, b: 2, c: 3, d: [], e: null, f: undefined, g: NaN }),
  JSON.myStringify({ a: 1, b: 2, c: 3, d: [], e: null, f: undefined, g: NaN })
);

https://gist.github.com/sahilmore-git/c261cde7909fc2fe102170fafbfe5631
https://gist.github.com/crates/904e45ea8f42a5522115488c6ea51bd7
