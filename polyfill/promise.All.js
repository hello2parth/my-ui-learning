// Implementing Promise.all using promise

function createAsyncTask() {
  const num = Math.floor(Math.random() * 10);
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (num > 2) {
        resolve(num);
      } else {
        reject(num);
      }
    }, num * 100);
  });
}

const aTask = [createAsyncTask(), createAsyncTask(), createAsyncTask(), createAsyncTask()];

function promiseAllPolyfill(params) {
  const aResult = [];
  let count = 0;
  return new Promise((resolve, reject) => {
    for (let i = 0; i < params.length; i++) {
      params[i]
        .then(val => {
          aResult[i] = val;
          count++;
          if (count === params.length) {
            resolve(aResult);
          }
        })
        .catch(err => {
          reject(err);
        });
    }
  });
}
promiseAllPolyfill(aTask)
  .then(v => console.log(`resolved ${v}`))
  .catch(v => console.log(`catched ${v}`));

// Implementing Promise.all without using promise

// function createAsyncTask() {
//   let num = Math.floor(Math.random() * 10);
//   return function asyncFun(cb) {
//     setTimeout(function () {
//       if(num > 2) {
//         cb(null,num);
//       } else {
//         cb(new Error(num));
//       }
//     });
//   }
// }

// let aTask = [createAsyncTask(),createAsyncTask(),createAsyncTask(),createAsyncTask()];

// function promiseAllPolyfill(params) {
//   let aResult = [];
//   let then = function(cb) {cb(aResult)};
//   // let _catch;
//   for(let i = 0; i < params.length; i++) {
//     params[i](function (err, data) {
//       if (err) {
//         // _catch = function(cb) {cb(err)};
//       }
//       aResult[i] = data;
//       if (aResult.length === params.length) {
//         then = function(cb) {cb(aResult)};
//         return {
//           then
//         }
//       }
//     })
//   }

// }
// var a = promiseAllPolyfill(aTask)
// a.then((v) => {
//   console.log(v);
// });
// a._catch((v) => {
//   console.log(v);
// });
