const string = 'parth';

const repeatThreeTimes = string => string.repeat(3);

const upperCase = string => string.toUpperCase();

const embolden = string => string.bold();

const repeatThreeTimesUpperCaseEmbolden = string => embolden(upperCase(repeatThreeTimes(string)));

console.log(repeatThreeTimesUpperCaseEmbolden(string));

function compose(...args) {
  return function (subject) {
    const aFun = args.reverse();
    return aFun.reduce((acc, val) => val(acc), subject);
  };
}

const result = compose(embolden, upperCase, repeatThreeTimes)(string);
console.log(result);
