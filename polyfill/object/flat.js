/* eslint-disable no-prototype-builtins */
/* eslint-disable no-restricted-syntax */
const user = {
  name: 'John',
  address: {
    primary: {
      house: '109',
      street: {
        main: '21',
        cross: 32,
        pincode: [1, 2, 3],
      },
    },
  },
};

if (!Object.prototype.flatten) {
  Object.defineProperty(Object.prototype, 'flatten', {
    value: function flatten(n = 0, defaultKey = '') {
      if (n > 0 && typeof this === 'object' && !Array.isArray(this)) {
        for (const key in this) {
          if (this.hasOwnProperty(key)) {
            const value = this[key];
            if (typeof value === 'object' && !Array.isArray(value)) {
              const result = value.flatten(n - 1, key);
              delete this[key];
              for (const key1 in result) {
                if (result.hasOwnProperty(key1)) {
                  this[defaultKey ? `${defaultKey}.${key1}` : key1] = result[key1];
                }
              }
            }
          }
        }
        return this;
      }
      return this;
    },
  });
}
console.log(user.flatten(5));
