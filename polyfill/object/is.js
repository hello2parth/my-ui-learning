if (!Object.myIs) {
  Object.defineProperty(Object, 'myIs', {
    value: (a, b) => {
      const getNegZero = value => 1 / value === -Infinity;
      const isANegZero = getNegZero(a);
      const isBNegZero = getNegZero(b);
      if (isANegZero || isBNegZero) {
        return isANegZero && isBNegZero;
      }
      if (Number.isNaN(a) && Number.isNaN(b)) {
        return a !== b;
      }
      return a === b;
    },
  });
}

console.log(Object.myIs(42, 42) === true);
console.log(Object.myIs('foo', 'foo') === true);
console.log(Object.myIs(false, false) === true);
console.log(Object.myIs(null, null) === true);
console.log(Object.myIs(undefined, undefined) === true);
console.log(Object.myIs(NaN, NaN) === true);
console.log(Object.myIs(-0, -0) === true);
console.log(Object.myIs(0, 0) === true);

console.log(Object.myIs(-0, 0) === false);
console.log(Object.myIs(0, -0) === false);
console.log(Object.myIs(0, NaN) === false);
console.log(Object.myIs(NaN, 0) === false);
console.log(Object.myIs(42, '42') === false);
console.log(Object.myIs('42', 42) === false);
console.log(Object.myIs('foo', 'bar') === false);
console.log(Object.myIs(false, true) === false);
console.log(Object.myIs(null, undefined) === false);
console.log(Object.myIs(undefined, null) === false);
