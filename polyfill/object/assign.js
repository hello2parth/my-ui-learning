Object.defineProperty(Object, 'myAssign', {
  value(target, ...source) {
    source.forEach(sourceObj => {
      const localSourceObj = Object(sourceObj);
      const keys = Object.keys(localSourceObj).concat(Object.getOwnPropertySymbols(localSourceObj));
      keys.forEach(key => {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(sourceObj, key));
      });
    });
    return target;
  },
});

/* Primitives will be wrapped to objects */
function validatePrimitives() {
  const target = { a: 5 };
  const result = Object.assign(target, { b: { c: 6 } }, { a: 7, [Symbol('test')]: 2 }, null, undefined, true, 'abc');
  const target1 = { a: 5 };
  const result1 = Object.myAssign(
    target1,
    { b: { c: 6 } },
    { a: 7, [Symbol('test')]: 2 },
    null,
    undefined,
    true,
    'abc'
  );

  console.log(result, target);
  console.log(result1, target1);
}

/* Properties on the prototype chain and non-enumerable properties cannot be copied */
function validateNonEnumerableProperties() {
  const obj = Object.create(
    { foo: 1 },
    {
      // foo is on obj's prototype chain.
      bar: {
        value: 2, // bar is a non-enumerable property.
      },
      baz: {
        value: 3,
        enumerable: true, // baz is an own enumerable property.
      },
    }
  );

  console.log({ ...obj });
  console.log(Object.myAssign({}, obj));
}

/* Exceptions will interrupt the ongoing copying task */
function validateInterruption() {
  const target1 = Object.defineProperty({}, 'foo', {
    value: 1,
    writable: false,
  }); // target1.foo is a read-only property

  try {
    Object.assign(target1, { bar: 2 }, { foo2: 3, foo: 3, foo3: 3 }, { baz: 4 });
    Object.myAssign(target1, { bar: 2 }, { foo2: 3, foo: 3, foo3: 3 }, { baz: 4 });
  } catch (e) {
    console.log(e);
  }

  console.log(target1.bar); // 2, the first source was copied successfully.
  console.log(target1.foo2); // 3, the first property of the second source was copied successfully.
  console.log(target1.foo); // 1, exception is thrown here.
  console.log(target1.foo3); // undefined, assign method has finished, foo3 will not be copied.
  console.log(target1.baz); // undefined, the third source will not be copied either.
}

/* accessors */

function validateAccessors() {
  const obj = {
    foo: 1,
    get bar() {
      return 2;
    },
  };

  console.log(Object.myAssign({}, obj));
}

validatePrimitives();
validateNonEnumerableProperties();
validateInterruption();
validateAccessors();
