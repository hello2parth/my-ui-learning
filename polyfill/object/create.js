Object.defineProperty(Object, 'myCreate', {
  value(o, properties) {
    function Foo() {}
    Foo.prototype = o;
    const newObj = new Foo();
    const keys = Object.keys(Object(properties));
    keys.forEach(key => {
      Object.defineProperty(newObj, key, properties[key]);
    });
    return newObj;
  },
});

const obj = { p: 2 };
const extendedObj = Object.create(obj, { q: { value: 3, enumerable: true }, r: { value: 4 } });
console.log(extendedObj, extendedObj.p);
const extendedObj1 = Object.myCreate(obj, { q: { value: 3, enumerable: true }, r: { value: 4 } });
console.log(extendedObj1, extendedObj1.p);
