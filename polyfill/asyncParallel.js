function createAsyncTask() {
  const num = Math.floor(Math.random() * 10);
  return function (taskID, cb) {
    setTimeout(() => cb(taskID, num), num * 100);
  };
}

const aTask = [createAsyncTask(), createAsyncTask(), createAsyncTask(), createAsyncTask()];

function performAsyncTask(taskList, cb) {
  const aResult = [];
  let counter = 0;
  for (let i = 0; i < taskList.length; i++) {
    taskList[i](i, (taskID, v) => {
      if (v < 2) {
        cb(Error(v));
      } else {
        aResult[taskID] = v;
        counter++;
        if (counter === taskList.length) {
          cb(null, aResult);
        }
      }
    });
  }
}

performAsyncTask(aTask, (error, data) => {
  if (error) {
    console.log(error);
  } else {
    console.log(data);
  }
});
// improve code: line #35 callback should be called once
// [callback should not be called once error is detected]
