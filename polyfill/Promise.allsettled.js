function asyncTask() {
  const num = Math.floor(Math.random() * 10);
  return function () {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (num > 5) {
          resolve(num);
        } else {
          reject(num);
        }
      }, num * 100);
    });
  };
}
const aTask = [asyncTask(), asyncTask(), asyncTask(), asyncTask()];

Promise.allSettled(aTask.map(v => v())).then(v => console.log(v));

// polyfill
if (!Promise.allSettledPolyfill) {
  Promise.allSettledPolyfill = function (aPromises) {
    return Promise.all(
      aPromises.map(promise =>
        promise.then(v => ({ status: 'fulfilled', value: v })).catch(v => ({ status: 'rejected', reason: v }))
      )
    );
  };
}
Promise.allSettledPolyfill(aTask.map(v => v())).then(v => console.log(v));
