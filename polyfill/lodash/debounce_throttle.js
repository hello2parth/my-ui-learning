/*
throttle: in t time, first call will be executed and rest calls will be ignored.

debounce:  Function call will be executed only when time difference of occurance between current & previous function call is more than debounce time
i.e t(f(n))-t(f(n-1)) > t (debounce time)
*/

const _ = (function lodash() {
  let id = -1;
  const register = {};

  return {
    debounce(fun, time) {
      id += 1;
      const currentId = id;
      return function debouncedFun(...args) {
        clearTimeout(register[currentId]?.timerId);
        register[currentId] = {
          timerId: setTimeout(() => {
            fun.apply(this, args);
          }, time),
        };
      };
    },

    throttle(fun, time) {
      id += 1;
      const currentId = id;
      return function throttledFun(...args) {
        if (!register[currentId]?.timerId) {
          fun.apply(this, args);
          register[currentId] = {
            timerId: setTimeout(() => {
              clearTimeout(register[currentId]?.timerId);
            }, time),
          };
        }
      };
    },
  };
})();

function abc() {
  console.log('I am called', this.i);
}
const deboucedFun1 = _.debounce(i => abc.call({ i }), 500);
deboucedFun1('debounce_1');
deboucedFun1('debounce_2');
deboucedFun1('debounce_3');

const debouncedFun2 = _.debounce(id => console.log('I am another debounce function', id), 500);
debouncedFun2(1);
debouncedFun2(2);

const throttledFun1 = _.throttle(i => abc.call({ i }), 500);
throttledFun1('throttle_1');
throttledFun1('throttle_2');
throttledFun1('throttle_3');
const throttledFun2 = _.throttle(id => console.log('I am another throttle function', id), 500);
throttledFun2(1);
throttledFun2(2);
