/* eslint-disable no-unused-vars */

const pubsub = (function () {
  const subscribers = {};
  const publish = function (subscriptionType, data) {
    if (subscribers[subscriptionType]) {
      subscribers[subscriptionType].forEach(v => {
        v.fun(data);
      });
    }
  };
  const subscribe = function (subscriptionType, cb) {
    if (!subscribers[subscriptionType]) {
      subscribers[subscriptionType] = [];
    }
    subscribers[subscriptionType].push({ id: subscriptionId, fun: cb });
    const subscriptionId = subscribers[subscriptionType].length;
    return { subscriptionType, subscriptionId };
  };
  const unsubscribe = function ({ subscriptionType, subscriptionId } = {}) {
    if (subscriptionId > -1 && subscribers[subscriptionType]) {
      const subsctptionData = subscribers[subscriptionType];
      const index = subsctptionData.findIndex(v => v.id === subscriptionId);
      subsctptionData.splice(index, 1);
      console.log(
        `Your Subscrition for ${subscriptionType} with subsctption id ${subscriptionId} is unsubscribed successfully.`
      );
    }
  };
  return {
    publish,
    subscribe,
    unsubscribe,
  };
})();

const sub1 = pubsub.subscribe('water', data => {
  console.log(`I am subscriber 1 and got the water, ${data}`);
});

const sub2 = pubsub.subscribe('water', data => {
  console.log(`I am subscriber 2 and got the water, ${data}`);
});

const sub3 = pubsub.subscribe('water', data => {
  console.log(`I am subscriber 3 and got the water, ${data}`);
});

const sub4 = pubsub.subscribe('water', data => {
  console.log(`I am subscriber 4 and got the water, ${data}`);
});

pubsub.publish('water', '100L');
pubsub.publish('gas', '15KG');

const sub5 = pubsub.subscribe('gas', data => {
  console.log(`I am subscriber 5 and got the gas, ${data}`);
});

pubsub.publish('water', '100L');
pubsub.publish('gas', '15KG');

const unsub1 = pubsub.unsubscribe(sub2);

pubsub.publish('water', '100L');
pubsub.publish('gas', '15KG');
