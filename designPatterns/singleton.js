/* The Singleton pattern is thus known because it restricts instantiation of a class to a single object. Classically, the Singleton pattern can be implemented by creating a class with a method that creates a new instance of the class if one doesn't exist. In the event of an instance already existing, it simply returns a reference to that object. */

const myfun = (function singleton() {
  let instance;
  function init() {
    const time = new Date();
    return {
      getTime() {
        return time;
      },
    };
  }
  return {
    getInstance() {
      if (!instance) {
        // if instance is already created then don't create it again, just call it.
        instance = init();
      }
      return instance;
    },
  };
})();

const time1 = myfun.getInstance();
const time2 = myfun.getInstance();

console.log(time1.getTime(), time2.getTime(), time1.getTime() === time2.getTime());
