const array = [
  [1, 2, 3, 4, 100],
  [5, 6, 7, 8, 200],
  [9, 10, 11, 12, 300],
  [13, 14, 15, 16, 400],
  ['a', 'b', 'c', 'd', 'e'],
];

function printSpiral(matrix) {
  const noOfCols = matrix[0].length;
  const noOfRows = matrix.length;
  if (noOfCols === noOfRows) {
    const noOfTimeToTraverse = [noOfCols, noOfRows - 1];
    const dir = [
      [0, 1], // right
      [1, 0], // down
      [0, -1], // left
      [-1, 0], // up
    ];

    let currentStep = [0, -1];
    let currentDir = 0;
    const getTraverseCount = () => noOfTimeToTraverse[currentDir % 2];
    while (getTraverseCount() > 0) {
      for (let i = 0; i < getTraverseCount(); i++) {
        currentStep = [currentStep[0] + dir[currentDir][0], currentStep[1] + dir[currentDir][1]];
        console.log(matrix[currentStep[0]][currentStep[1]]);
      }
      noOfTimeToTraverse[currentDir % 2] = getTraverseCount() - 1;
      currentDir = (currentDir + 1) % dir.length;
    }
  }
}

printSpiral(array);
