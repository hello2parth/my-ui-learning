/*
Please ensure that before submitting, you do not have any console logs in your solution.
 */

const getRateLimiter = (apiService, limit) => {
  let i = 0;
  let j = 0;
  let localLimit = limit;
  const requestIDs = [];
  const send = requestId => {
    if (!requestIDs.length || !requestIDs.some(v => v === requestId)) {
      requestIDs.push(requestId);
    }
    return new Promise(() => {
      if (i < localLimit) {
        apiService(requestId).then((response)=>{
          console.log(response);
          j++;
          if(j === localLimit && j < requestIDs.length) {
            localLimit += limit;
            for (let k = j; k < localLimit && k < requestIDs.length; k++) {
              send(requestIDs[k]);
            }
          }
        });
        i++;
      }
    });
  }
  return { send }; // Do not change the return  type
};










/*
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
// Do not change below code or you will be disqualified.
// We suggest you to go through the below code to debug issues.
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
*/

function main(input) {
  const lines = input.split("\n");
  const limit = parseInt(lines[0]);
  const requestTuples = lines.slice(1, lines.length).map(tupleStr => {
      let [id, duration] = tupleStr.split(" ");
      id = id.trim();
      duration = parseInt(duration);
      return { id, duration };
  });

  runner(limit, requestTuples);
}

const runner = (limit, requestTuples) => {
  const batchLog = [];

  const apiService = requestID => {
      const log = `START ${requestID}`;
      console.log(log);
      return new Promise((resolve, reject) => {
          const info = requestTuples.find(item => item.id === requestID);
          setTimeout(function() {
              const finishLog = `FINISH ${info.id}`;
              resolve(finishLog);
          }, info.duration * 100);
      });
  };
  const rateLimiter = getRateLimiter(apiService, limit);

  // Runner is going to call send for all API requests at once and in the same order
  // as input testcase
  Promise.all(
      requestTuples.map(r => {
          const requestID = r.id;

          // The send function is the one returned from getRateLimiter()
          return rateLimiter.send(requestID).then(response => console.log(response));
      })
  );
};

// Input parsing
// process.stdin.resume();
// process.stdin.setEncoding("utf-8");
let stdin_input = `3
A 8
B 6
C 4
D 6
E 2
F 5
G 4
H 5
I 8`;
// process.stdin.on("data", function(input) {
//   stdin_input += input; // Reading input from STDIN
// });
main(stdin_input);
// process.stdin.on("end", function() {
//   main(stdin_input);
// });
