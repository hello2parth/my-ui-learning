function orderStringCharInAscendingOrder(value = '') {
  return value.toLowerCase().split('').sort().join('');
}

function isAnargram(a, b) {
  return orderStringCharInAscendingOrder(a) === orderStringCharInAscendingOrder(b);
}

console.log(isAnargram('School master', 'The classroom'));
console.log(isAnargram('peon', 'neon'));
