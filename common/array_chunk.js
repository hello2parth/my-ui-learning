const num = [2, 3, 4, 5, 6, 12, 345, 12, 1, 3, 345, 345, 12, 13, 12, 1, 1, 2, 3, 4, 2];

function chunkArray(array, size, result = []) {
  if (array.length > size) {
    result.push(array.slice(0, size));
    chunkArray(array.slice(0 + size, array.length), size, result);
  } else {
    result.push(array);
  }
  return result;
}
console.log(chunkArray(num, 3));
