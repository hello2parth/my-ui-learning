/* eslint-disable eqeqeq */

const a = {
  getValue: (function* getValue() {
    for (let i = 1; i < Infinity; i++) {
      yield i;
    }
  })(),
};

Object.defineProperty(a, 'valueOf', {
  value() {
    return a.getValue.next().value;
  },
});

if (a == 1 && a == 2 && a == 3) {
  console.log('You are hired');
}
