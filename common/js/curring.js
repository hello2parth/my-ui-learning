function sum(a) {
  return function inner(b) {
    if (!b) {
      return a;
    }
    return sum(a + b);
  };
}

console.log(sum(4)(6)(8)(10)());

// ES6 solution
const add = a => b => b ? add(a + b) : a;
console.log(add(4)(6)(8)(10)());
