/* eslint-disable no-alert */
/* eslint-disable max-classes-per-file */
function seq() {
  let i = 0;
  return function () {
    return i++;
  };
}
const seq1 = seq();
const seq2 = seq();

alert(seq1()); // line 1
alert(seq1()); // line 2
alert(seq2()); // line 3

JSON.stringify(1 / 0);

class DS {
  constructor(x) {
    this.x = x;
  }

  static addDSTest(f, b) {
    return f.x + b.x + b.y;
  }
}
class Test extends DS {
  constructor(x, y) {
    super(x);
    this.y = y;
  }
}
const f = new DS(5);
const b = new Test(5, 5);
console.log(Test.addDSTest(f, b));
