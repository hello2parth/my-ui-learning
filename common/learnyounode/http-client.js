'use strict';
const http = require('http');

http.get(process.argv[2], 'utf-8', (response) => {
  let rawData = '';
  response.on('data', (data) => rawData = rawData + data + '\n');
  response.on('end', () => console.log(rawData));
  response.on('error', (e) => console.log(e));
}).on('error', console.error);
