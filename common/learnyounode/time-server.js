'use strict';

const net = require('net');
const server = net.createServer((socket) => {
  let date = new Date();
  let data = `${date.getFullYear()}-${String(date.getMonth()  + 1 ).length === 1 ? '0' + String(date.getMonth()  + 1 ) : String(date.getMonth()  + 1 )}-${date.getDate()} ${date.getHours()}:${date.getMinutes() + '\n'}`;

  socket.end(data);
});
server.listen(process.argv[2]);
