const fs = require('fs');

// fs.open('C:\\Users\\papatel\\Desktop\\Learning\\node\\my-first-io.js',(error, data) => {
// 	if(error) {
// 		if(error.code === 'ENOENT') {
// 			console.error("My file doesn't exist");
// 			return;
// 		}
// 		throw error;
// 	}
// 	console.log(data);
// })

// fs.readFile('C:\\Users\\papatel\\Desktop\\Learning\\node\\my-first-io.js', 'utf8', function(err, data) {
// 	if (err) throw err;
// 	if (!!data) {
// 		var regex = /\n/gi, result, indices = [];
// 		while ( (result = regex.exec(data)) ) {
// 				indices.push(result.index);
// 		}
// 		console.log(indices.length + 1);
// 	}
// });

try {
	var data = fs.readFileSync(process.argv[2], 'utf8');
	if (!!data) {
		var regex = /\n/gi, result, indices = [];
		while ( (result = regex.exec(data)) ) {
				indices.push(result.index);
		}
		console.log(indices.length);
	}
} catch(e) {
	console.log('Error:', e.stack);
}

// console.log(fs.readFileSync(process.argv[2], 'utf8').split('\n').length - 1); // easier way shown in learnyounode
