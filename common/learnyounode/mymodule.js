const fs = require('fs');
const path = require('path');
module.exports = function (dirPath, ext, cb) {
  fs.readdir(dirPath, (error, data) => {
    if(error) return cb(error);
    let userExt = '.' + ext;
    const array = [];
    data.forEach((v) => {
      let extension = path.extname(v);
      if(extension === userExt) {
        // console.log(v);
        array.push(v);
      }
    })
    cb(null,array);
  });
}
