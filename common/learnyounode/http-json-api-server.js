const http = require('http');
const url = require('url');

http.createServer((request, response) => {
  if (request.method === 'GET') {
    let data = '';
    const parsedURL = url.parse(request.url);
    let isoDate = parsedURL.query.split('iso=')[1];
    let date =  new Date(isoDate);
    if (parsedURL.pathname === '/api/parsetime') {
      data = {
        "hour": date.getHours(),
        "minute": date.getMinutes(),
        "second": date.getSeconds()
      };
    } else if (parsedURL.pathname === '/api/unixtime') {
      data = { "unixtime": +date };
    }
    response.writeHead(200, {'Content-Type': 'application/json'});
    response.write(JSON.stringify(data));
    response.end();
    // console.log(parsedURL.query);
  } else {
    response.writeHead(404)
    response.end()
  }
}).listen(process.argv[2]);
