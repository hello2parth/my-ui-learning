'use strict';
const http = require('http');

function callHttp(url, server) {
  http.get(url,'utf-8',(response) => {
    let rawData = '';
    response.on('data', (data) => {rawData += data;});
    response.on('end', () => {printResponse(rawData, server);});
    response.on('error', (e) => {console.log(e);});
  }).on('error', console.error);
}

for (let i=2; i<5; i++) {
  callHttp(process.argv[i], i-1);
}

let data1 = '', data2 ='', data3 ='';
function printResponse(data, server) {
  switch(server) {
    case 1:
      data1 = data;
      break;
    case 2:
      data2 = data;
      break;
    case 3:
      data3 = data;
      break;
  }
  if (data1 && data2 && data3) {
    console.log(data1 + '\n' + data2 + '\n' + data3);
  }
};
// let response1 = new Promise((resolve) => {
//   http.get(process.argv[2],'utf-8',(response) => {
//     let rawData = '';
//     response.on('data', (data) => {rawData += data;});
//     response.on('end', () => {resolve(rawData);});
//     response.on('error', (e) => {console.log(e);});
//   }).on('error', console.error);
// });

// let response2 = new Promise((resolve) => {
//   http.get(process.argv[3],'utf-8',(response) => {
//     let rawData = '';
//     response.on('data', (data) => {rawData += data;});
//     response.on('end', () => {resolve(rawData);});
//     response.on('error', (e) => {console.log(e);});
//   }).on('error', console.error);
// });

// let response3 = new Promise((resolve) => {
//   http.get(process.argv[4],'utf-8',(response) => {
//     let rawData = '';
//     response.on('data', (data) => {rawData += data;});
//     response.on('end', () => {resolve(rawData);});
//     response.on('error', (e) => {console.log(e);});
//   }).on('error', console.error);
// });


// Promise.all([response1,response2, response3]).then((result) => {
//   console.log(result[0] + '\n' + result[1] + '\n' + result[2]);
// })

