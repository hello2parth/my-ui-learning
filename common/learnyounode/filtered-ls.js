const fs = require('fs');
const path = require('path');

fs.readdir(process.argv[2],(error, data) => {
  if(error) {
    throw error;
  }
  data.forEach((v) => {
    let extension = path.extname(v);
    if(extension === ('.'+ process.argv[3])) {
      console.log(v);
    }
  })
})
