const http =  require('http');
const map = require('through2-map');

http.createServer((request, response) => {
  if (request.method === 'POST') {
    response.writeHead(200, {'Content-Type': 'text/plain'})
    // request.pipe(map(function (chunk) {
    //   return chunk.toString().toUpperCase();
    // })).pipe(response);
    let data = '';
    request.on('data', (chunk) => {
      data += chunk.toString().toUpperCase();
    });

    request.on('end', () => {
      response.end(data);
    });
  } else {
    response.end('send me a POST\n');
  }
}).listen(process.argv[2]);
