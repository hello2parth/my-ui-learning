// is it possible to get array of zeros by subtracting the one from i and i+1 th index where 0 <i<array.length-1 ?

const aNum = [0, 3, 8, 6, 5, 4];

let flag = true;
for (let i = 0; i < aNum.length - 1; i++) {
  if (aNum[i + 1] - aNum[i] >= 0) {
    aNum[i + 1] = aNum[i + 1] - aNum[i];
    aNum[i] = 0;
  } else {
    flag = false;
    break;
  }
}
if (flag) {
  if (aNum[aNum.length - 1] === 0) {
    console.log('Yes', aNum);
  }
} else {
  console.log('No', aNum);
}
