const num = [2, 3, 4, 5, 6, 12, 345, 12, 1, 3, 345, 345, 12, 13, 13, 13, 12, 1, 1, 2, 3, 4, 2];

// min and max using Math

console.log('min/smallest num', Math.min.apply(Math, num));
console.log('max/largest num', Math.max.apply(Math, num));

// maximum and minimum without using array sort

function getMaxAndMin(array) {
  let max = array[0];
  let min = array[0];

  for (let i = 0; i < array.length; i++) {
    if (array[i] > max) {
      max = array[i];
    }
    if (array[i] < min) {
      min = array[i];
    }
  }
  return { max, min };
}
console.log('max and min without sort', getMaxAndMin(num));

// seonnd max and min without sort

function get2ndMaxandMin(array) {
  if (array.length > 2) {
    let max;
    let secondMax;
    let min;
    let secondMin;
    max = secondMax = min = secondMin = array[0];
    for (let i = 0; i < array.length; i++) {
      const currentNum = array[i];
      // max
      if (currentNum > max && currentNum > secondMax) {
        secondMax = max;
        max = currentNum;
      } else if (currentNum < max && currentNum > secondMax) {
        secondMax = currentNum;
      }
      // min
      if (currentNum < min && currentNum < secondMin) {
        secondMin = min;
        min = currentNum;
      } else if (currentNum > min && currentNum < secondMin) {
        secondMin = currentNum;
      }
    }
    return { max, secondMax, min, secondMin };
  }
}

console.log('second max without sort', get2ndMaxandMin(num));
// Math and min using Array sort

const numCopy = JSON.parse(JSON.stringify(num));
numCopy.sort((a, b) => {
  if (a > b) {
    return 1;
  }
  if (a < b) {
    return -1;
  }
  return 1;
});
// console.log(numCopy);
console.log('min and max are', numCopy[0], numCopy[numCopy.length - 1]);
// max num
function getKthMaxMin(arr, k) {
  let max;
  let min;
  let maxCount = 0;
  let minCount = 0;
  for (let i = arr.length - 1; i >= 0; i--) {
    if (arr[i] !== arr[i - 1]) {
      maxCount++;
      if (maxCount === k) {
        max = arr[i];
        break;
      }
    }
  }
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] !== arr[i + 1]) {
      minCount++;
      if (minCount === k) {
        min = arr[i];
        break;
      }
    }
  }
  return { min, max };
}

console.log('3nd min and max are', getKthMaxMin(numCopy, 3));

const bubbleInsert = require('./BinarySort');

// console.log(bubbleInsert);

function getKthMaxWithoutSort(array, k) {
  if (array.length >= k) {
    const maxArray = new Array(k);
    maxArray.fill(null);
    for (let i = 0; i < array.length; i++) {
      const currentNum = array[i];
      if (maxArray[k - 1] === null || currentNum > maxArray[k - 1]) {
        for (let j = 0; j < maxArray.length; j++) {
          if (currentNum === maxArray[j]) {
            break;
          } else if (currentNum > maxArray[j]) {
            if (j === 0) {
              maxArray.unshift(currentNum);
            } else {
              maxArray.splice(j, 0, currentNum);
            }
            maxArray.pop();
            break;
          } else if (maxArray[j] === null) {
            maxArray[j] = currentNum;
            break;
          }
        }
      }
    }
    return maxArray[k - 1];
  }
}
// # Write a function that accepts an array of n integers and a integer k, and returns the kth largest number in the array
// # k << n
// # n = 10M and k = 7
function getKthMaxWithoutSortandBubbleInsert(array, k) {
  if (array.length >= k) {
    const maxArray = new Array(k);
    maxArray.fill(null);
    for (let i = 0; i < array.length; i++) {
      const currentNum = array[i];
      if (maxArray[k - 1] === null) {
        for (let j = 0; j < maxArray.length; j++) {
          if (currentNum === maxArray[j]) {
            break;
          } else if (maxArray[j] === null) {
            maxArray[j] = currentNum;
            break;
          } else if (currentNum > maxArray[j]) {
            if (j === 0) {
              maxArray.unshift(currentNum);
            } else {
              maxArray.splice(j, 0, currentNum);
            }
            maxArray.pop();
            break;
          }
        }
      } else if (currentNum > maxArray[0]) {
        maxArray.unshift(currentNum);
        maxArray.pop();
      } else if (currentNum > maxArray[k - 1]) {
        const position = bubbleInsert(maxArray, currentNum);
        if (position !== null) {
          maxArray.splice(position + 1, 0, currentNum);
          maxArray.pop();
        }
      }
    }
    return maxArray[k - 1];
  }
}

console.log('3nd max is', getKthMaxWithoutSort(num, 3));
console.log('3nd max is', getKthMaxWithoutSortandBubbleInsert(num, 3));
console.log('2nd max is', getKthMaxWithoutSort([2, 1], 2));
console.log('2nd max is', getKthMaxWithoutSortandBubbleInsert([2, 1], 2));
console.log('2nd max is', getKthMaxWithoutSort([-1, 2, 0], 2));
console.log('2nd max is', getKthMaxWithoutSortandBubbleInsert([-1, 2, 0], 2));
// [2,1], 2
