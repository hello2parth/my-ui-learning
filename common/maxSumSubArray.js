/* Print continuous subarray with maximum sum
Given an integer array, find and print a contiguous subarray with the maximum sum in it.

For example,

Input:  {-2, 1, -3, 4, -1, 2, 1, -5, 4}

Output: The contiguous subarray with the largest sum is {4, -1, 2, 1}


Input:  {8, -7, -3, 5, 6, -2, 3, -4, 2}

Output: The contiguous subarray with the largest sum is {5, 6, -2, 3} */

function findMaxSumSubArray(array) {
  let maxTillLastIndex = array[0];
  let globalMax = -Infinity;
  let start = 0;
  let end = 1;

  for (let i = 1; i < array.length; i++) {
    const currentElement = array[i];
    const maxSumAtCurrentIndex = Math.max(currentElement, currentElement + maxTillLastIndex);
    if (currentElement > currentElement + maxTillLastIndex) {
      start = i;
    }
    if (maxSumAtCurrentIndex >= globalMax) {
      globalMax = maxSumAtCurrentIndex;
      end = i;
    }
    maxTillLastIndex = maxSumAtCurrentIndex;
  }
  return { globalMax, array: array.slice(start, end + 1) };
}

console.log(findMaxSumSubArray([-2, 1, -3, 4, -1, 2, 1, -5, 4]));
