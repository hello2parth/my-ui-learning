const refTime = '23:05:38';

const aTime = ['23:05:38', '23:05:02', '23:04:59', '23:04:31', '12:36:07', '08:59:01', '00:00:00'];

const aRefSplitedTime = refTime.split(':').map(v => parseInt(v, 10));
const refSec = aRefSplitedTime[0] * 60 * 60 + aRefSplitedTime[1] * 60 + aRefSplitedTime[2];

aTime.forEach(v => {
  const aSpiltedTime = v.split(':').map(v => parseInt(v, 10));
  const vSec = aSpiltedTime[0] * 60 * 60 + aSpiltedTime[1] * 60 + aSpiltedTime[2];
  const diff = refSec - vSec;
  if (diff === 0) {
    console.log('Reference time', refTime, 'compared time', v, 'now');
  } else if (diff > 0 && diff < 60) {
    console.log('Reference time', refTime, 'compared time', v, diff, 'seconds ago');
  } else if (diff >= 60 && diff < 60 * 60) {
    console.log('Reference time', refTime, 'compared time', v, Math.floor(diff / 60), 'minutes ago');
  } else if (diff >= 60 * 60 && diff < 60 * 60 * 60) {
    console.log('Reference time', refTime, 'compared time', v, Math.floor(diff / (60 * 60)), 'hours ago');
  }
});
