/* eslint-disable no-prototype-builtins */

/* Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.
You may assume that each input would have exactly one solution, and you may not use the same element twice.
array [2, 7, 11, 15]
target 9
You can return the answer in any order. */

function sumMatchTarget(nums, target) {
  const targetSum = {};
  for (let i = 0; i < nums.length; i++) {
    const firstOperand = nums[i];
    if (targetSum.hasOwnProperty(firstOperand)) {
      return [targetSum[firstOperand], i];
    }
    const secondOperand = target - firstOperand;
    targetSum[secondOperand] = i;
  }
  return [-1, -1];
}

const nums = [2, 7, 11, 15];
const target = 9;
console.log(sumMatchTarget(nums, target));
