const input = [1, 4, 2, 6, 4, 6, 2, 1];

function bubbleSort(arr) {
  for (let i = 0; i < arr.length; i++) {
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[j] < arr[i]) {
        const current = arr[j];
        const previous = arr[i];
        arr[i] = current;
        arr[j] = previous;
      }
    }
  }
  return arr;
}

console.log(bubbleSort(input));

module.exports = bubbleSort;

// Algorithm	Time Complexity
//   Best	Average	Worst
// Selection Sort	Ω(n^2)	θ(n^2)	O(n^2)
// Bubble Sort	Ω(n)	θ(n^2)	O(n^2)
// Insertion Sort	Ω(n)	θ(n^2)	O(n^2)
// Heap Sort	Ω(n log(n))	θ(n log(n))	O(n log(n))
// Quick Sort	Ω(n log(n))	θ(n log(n))	O(n^2)
// Merge Sort	Ω(n log(n))	θ(n log(n))	O(n log(n))
// Bucket Sort	Ω(n+k)	θ(n+k)	O(n^2)
// Radix Sort	Ω(nk)	θ(nk)	O(nk)

// https://www.geeksforgeeks.org/time-complexities-of-all-sorting-algorithms/
// https://codingmiles.com/sorting-algorithms-bubble-sort-using-javascript/
