const num = [11, 12, 13, 14, 15, 16, 18, 19];

function binarySort(array, value) {
  const len = array.length;
  let start = 0;
  let end = len - 1;
  let mid;
  let position;
  while (end > start) {
    mid = Math.floor((start + end) / 2);
    if (value === array[mid]) {
      position = mid;
      break;
    } else if (value > array[mid]) {
      start = mid;
    } else {
      end = mid;
    }
  }
  return position;
}
console.log(binarySort(num, 11));

function binarySortInsert(array, value) {
  const len = array.length;
  let start = 0;
  let end = len - 1;
  let mid;
  let position;
  while (end > start) {
    mid = Math.floor((start + end) / 2);
    if (value === array[mid]) {
      position = null; // already exist
      break;
    } else if (value > array[mid]) {
      if (value < array[mid + 1]) {
        position = mid;
        break;
      }
      start = mid;
    } else {
      if (value > array[mid - 1]) {
        position = mid - 1;
        break;
      }
      end = mid;
    }
  }
  return position;
}
console.log(binarySortInsert(num, 17));

function binaryInsertReverseSorted(array, value) {
  const len = array.length;
  let start = 0;
  let end = len - 1;
  let mid;
  let position;
  while (end > start) {
    mid = Math.floor((start + end) / 2);
    if (value === array[mid]) {
      position = null; // already exist
      break;
    } else if (value < array[mid]) {
      if (value > array[mid + 1]) {
        position = mid;
        break;
      }
      start = mid;
    } else {
      if (value < array[mid - 1]) {
        position = mid - 1;
        break;
      }
      end = mid;
    }
  }
  return position;
}
module.exports = binaryInsertReverseSorted;
const num1 = [345, 12, 6, 5, 3, 2];
console.log(binaryInsertReverseSorted(num1, 13));
