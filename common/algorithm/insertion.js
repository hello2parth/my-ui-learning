const array = [4, 6, 4, 3, 9, 2, 1];

for (let j = 1; j < array.length; j++) {
  let i = j - 1;
  const currentSortedIndex = j;
  while (i >= 0 && array[j] < array[i]) {
    const next = array[j];
    const previous = array[i];
    array[i] = next;
    array[j] = previous;
    i--;
    j--;
  }
  j = currentSortedIndex;
}
console.log(array);
