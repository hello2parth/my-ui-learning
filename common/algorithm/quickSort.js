const test = [2, 3, 4, 5, 6, 12, 345, 12, 1, 3, 345, 345, 12, 13, 12, 1, 1, 2, 3, 4, 2];

function quickSort(items, leftIndex = 0, rightIndex = items.length - 1) {
  if (items.length > 1) {
    const pivotIndex = partition(items, leftIndex, rightIndex);
    if (leftIndex < pivotIndex - 1) {
      quickSort(items, leftIndex, pivotIndex - 1);
    }
    if (pivotIndex < rightIndex) {
      quickSort(items, pivotIndex, rightIndex);
    }
  }
  return items;
}
function partition(items, leftIndex, rightIndex) {
  const pivot = items[Math.floor((leftIndex + rightIndex) / 2)]; // pivotIndex
  while (leftIndex <= rightIndex) {
    while (items[leftIndex] < pivot) {
      leftIndex += 1;
    }
    while (pivot < items[rightIndex]) {
      rightIndex -= 1;
    }
    if (leftIndex <= rightIndex) {
      [items[rightIndex], items[leftIndex]] = [items[leftIndex], items[rightIndex]];
      leftIndex += 1;
      rightIndex -= 1;
    }
  }
  return leftIndex;
}
console.log(quickSort(test));
