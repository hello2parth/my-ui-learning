const num = [9, 4, 1, 7, 9, 1, 2, 0];

const countSort = (array, max = Math.max.apply(null, array)) => {
  const sortedArray = Array(max + 1).fill(array[array.length - 1]);
  array.forEach(v => {
    sortedArray[v] += 1;
  });
  console.log({ sortedArray });
  for (let i = 1; i < sortedArray.length; i++) {
    sortedArray[i] += sortedArray[i - 1];
  }
  return sortedArray;
};

console.log(countSort(num));
