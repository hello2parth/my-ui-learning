const num = [2, 3, 4, 5, 6, 12, 345, 12, 1, 3, 345, 345, 12, 13, 12, 1, 1, 2, 3, 4, 2];

function mergeSort(array) {
  if (array.length > 1) {
    const halfLength = Math.floor(array.length / 2);
    const leftHalf = array.slice(0, halfLength);
    const rightHalf = array.slice(halfLength, array.length);
    return merge(mergeSort(leftHalf), mergeSort(rightHalf));
  }
  return array;
}

function merge(leftHalf, rightHalf) {
  const result = [];
  while (leftHalf.length && rightHalf.length) {
    if (leftHalf[0] <= rightHalf[0]) {
      result.push(leftHalf.shift());
    } else {
      result.push(rightHalf.shift());
    }
  }
  while (leftHalf.length) {
    result.push(leftHalf.shift());
  }
  while (rightHalf.length) {
    result.push(rightHalf.shift());
  }
  return result;
}

console.log(mergeSort(num));

module.exports = mergeSort;
