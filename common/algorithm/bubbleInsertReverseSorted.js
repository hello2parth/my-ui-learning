function BubbleInsertReverseSorted(array, value) {
  const len = array.length;
  let start = 0;
  let end = len - 1;
  let mid;
  let position;
  while (end > start) {
    mid = Math.floor((start + end) / 2);
    if (value === array[mid]) {
      position = mid; // already exist
      break;
    } else if (value < array[mid]) {
      if (value > array[mid + 1]) {
        position = mid;
        break;
      }
      start = mid;
    } else {
      if (value < array[mid - 1]) {
        position = mid - 1;
        break;
      }
      end = mid;
    }
  }
  return position;
}
function getKthMaxWithoutSortandBubbleInsert(array, k) {
  if (array.length >= k) {
    const maxArray = new Array(k);
    maxArray.fill(null);
    for (let i = 0; i < array.length; i++) {
      const currentNum = array[i];
      if (maxArray[k - 1] === null) {
        for (let j = 0; j < maxArray.length; j++) {
          if (currentNum > maxArray[j]) {
            if (j === 0) {
              maxArray.unshift(currentNum);
            } else {
              maxArray.splice(j, 0, currentNum);
            }
            maxArray.pop();
            break;
          } else if (maxArray[j] === null) {
            maxArray[j] = currentNum;
            break;
          }
        }
      } else if (currentNum > maxArray[0]) {
        maxArray.unshift(currentNum);
        maxArray.pop();
      } else if (currentNum > maxArray[k - 1]) {
        const position = BubbleInsertReverseSorted(maxArray, currentNum);
        maxArray.splice(position + 1, 0, currentNum);
        maxArray.pop();
      }
    }
    return maxArray;
  }
}
// console.log('2nd max is', getKthMaxWithoutSortandBubbleInsert([-1,-1], 2));
console.log('4nd max is', getKthMaxWithoutSortandBubbleInsert([-1, 2, 0], 2));
