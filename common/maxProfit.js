// Asked in avtar

// Given a stock’s price on N days, return the maximum possible profit a trader can make. At most one transaction is allowed. You cannot sell a stock before you buy one.
// Input: [7,1,5,3,6,4]
// Output: 5
// Explanation: Buy on day 2 (price = 1) and sell on day 5 (price = 6), profit = 6-1 = 5.

function getMaxProfit(priceArray) {
  let buyPrice = priceArray[0];
  let sellPrice = -1;
  let maxProfit = 0;
  for (let i = 1; i < priceArray.length; i++) {
    const currentSellPrice = priceArray[i];
    let currentProfit = currentSellPrice - buyPrice;
    if (currentProfit < 0) {
      buyPrice = currentSellPrice;
      currentProfit = currentSellPrice - buyPrice;
    }
    if (currentProfit > maxProfit) {
      sellPrice = currentSellPrice;
      maxProfit = currentProfit;
    }
  }
  return {
    buyPrice,
    sellPrice,
    maxProfit,
  };
}

console.log(getMaxProfit([10, 9, 11, 18, 17]));
console.log(getMaxProfit([7, 1, 5, 3, 6, 4]));
