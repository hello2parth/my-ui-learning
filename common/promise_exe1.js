function job(state) {
  return new Promise((resolve, reject) => {
    if (state) {
      resolve('Promise success');
    } else {
      reject('Promise error');
    }
  });
}
const promise = job(true);

promise
  .then(data => {
    console.log(data);
    return job(false);
  })

  .catch(error => {
    console.log(error);
    return 'Promise Error caught';
  })

  .then(data => {
    console.log(data);
    return job(true);
  })

  .catch(error => {
    console.log(error);
  });

/// ////////////////////

const demo = new Promise(resolve => {
  throw 'Hello World';
});
demo.catch(() => 'Hello World 2').then(console.log.bind(console));
