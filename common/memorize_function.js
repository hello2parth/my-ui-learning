function memorise(fun) {
  const oCache = {};
  return function () {
    const args = JSON.stringify(arguments);
    if (!oCache[args]) {
      oCache[args] = fun.apply(this, arguments);
    }
    return oCache[args];
  };
}

function factorial(n) {
  if (n < 2) {
    return 1;
  }
  return n * factorial(n - 1);
}
const memorisedFactorial = memorise(factorial);
console.log(memorisedFactorial(100)); // slow
console.log(memorisedFactorial(100));
