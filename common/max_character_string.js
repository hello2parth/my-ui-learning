/* eslint-disable no-prototype-builtins */
/* eslint-disable no-restricted-syntax */
function maxChar(string) {
  const result = {};
  for (let i = 0; i < string.length; i++) {
    const char = string.charAt(i);
    if (char !== '' && !result[char]) {
      const regex = new RegExp(char, 'g');
      result[char] = string.match(regex).length;
    }
  }
  const aCount = Object.values(result);
  const maxCount = Math.max(...aCount);
  const max = [];
  for (const key in result) {
    if (result.hasOwnProperty(key)) {
      if (result[key] === maxCount) {
        max.push(key);
      }
    }
  }
  return { result, maxCount, max };
}

console.log(maxChar('parthkumar manubhai patel'));
