function Stopwatch(context) {
  let duration = 0;
  let startedTime = 0;
  let stopedTime = 0;

  this.city = 'ABC';

  this.start = function () {
    if (startedTime) {
      throw new Error('Stop watch already started');
    } else {
      startedTime = +new Date();
    }
  };
  this.stop = function () {
    if (startedTime) {
      stopedTime = +new Date();
    } else {
      throw new Error("Stop watch haven't started");
    }
  };
  this.reset = function () {
    duration = 0;
    startedTime = 0;
    stopedTime = 0;
  };
  Object.defineProperty(this, 'duration', {
    get() {
      return `${(stopedTime - startedTime) / 1000}ms`;
    },
  });
  console.log(this);
}

const sw = new Stopwatch();
sw.start();
setTimeout(() => {
  sw.stop();
  console.log(sw.duration);
  console.log(sw.city);
}, 2000);

const sw1 = Stopwatch();
sw1.start();
setTimeout(() => {
  sw1.stop();
  console.log(sw1.duration);
  console.log(sw1.city);
}, 1000);
