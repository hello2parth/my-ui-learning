function fibonacci() {
  const aResult = [0, 1];
  return function fibonacciPrint(n) {
    const arrayLength = aResult.length;
    if (arrayLength > n) {
      return aResult;
    }
    const last = aResult[arrayLength - 1];
    const secondLast = aResult[arrayLength - 2];
    aResult.push(last + secondLast);
    return fibonacciPrint(n);
  };
}
const fibonacciFun = fibonacci();
const result = fibonacciFun(25);
console.log(result);

// without using array
function fibonacciWArray(n) {
  let count = 0;
  let last = 0;
  let secondLast = 0;
  while (count < n) {
    if (count === 0) {
      last = 0;
      secondLast = 0;
    } else if (count === 1) {
      last = 1;
      secondLast = 0;
    } else {
      [last, secondLast] = [last + secondLast, last];
    }
    count += 1;
  }
  return last + secondLast;
}
console.log(fibonacciWArray(25));
