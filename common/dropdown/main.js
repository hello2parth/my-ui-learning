function expand() {
  addItem();
  const oList = document.getElementById('dropdown-list');
  if (oList.classList.contains('hide')) {
    oList.classList.remove('hide');
    oList.classList.add('show');
  } else {
    oList.classList.add('hide');
    oList.classList.remove('show');
  }
}
function onItemSelection(event) {
  const target = getEventTarget(event);
  document.getElementById('select-input').value = target.innerText;
}
function getEventTarget(e) {
  e = e || window.event;
  return e.target || e.srcElement;
}
function addItem() {
  const aItems = [2, 3, 45, 6, 7, 8, 9];
  const oUl = document.getElementById('ul-list');
  let oLi = '';
  for (let i = 0; i < aItems.length; i++) {
    oLi += `<li>${aItems[i]}</li>`;
  }
  oUl.innerHTML = oLi;
}
