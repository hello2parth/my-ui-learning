const oTemplate = document.createElement('template');
oTemplate.innerHTML = `
  <style>
    .show {
      display: inline;
    }
    .hide {
      display: none;
    }
    ul {
      list-style: none;
      margin: 0;
      padding: 0;
      height: 100px;
      overflow-y: auto;
      width: 100px;
    }
    ul li {
      padding-left: 5px;
    }
    ul li:nth-of-type(odd) {
      background-color: gray;
    }
    ul li:nth-of-type(even) {
      background-color: coral;
    }
  </style>
  <div class="drop-down">
    <input id="dd-input">
    <div id="dd-list" class="hide">
      <ul id="dd-ul">
      </ul>
    </div>
  </div>
`;
class DropDown extends HTMLElement {
  constructor() {
    super();
    this.isShow = false;
    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(oTemplate.content.cloneNode(true));
  }

  toggleDropdown() {
    const oDDList = this.shadowRoot.getElementById('dd-list');
    if (this.isShow) {
      oDDList.classList.add('hide');
      oDDList.classList.remove('show');
    } else {
      oDDList.classList.remove('hide');
      oDDList.classList.add('show');
    }
    this.isShow = !this.isShow;
  }

  connectedCallback() {
    this.shadowRoot.getElementById('dd-input').addEventListener('click', () => this.toggleDropdown());
    const oUl = this.shadowRoot.getElementById('dd-ul');
    const options = JSON.parse(this.getAttribute('options'));
    let oLi = '';
    for (let i = 0; i < options.length; i++) {
      oLi += `<li>${options[i]}</li>`;
    }
    oUl.innerHTML = oLi;
    console.log(options);
  }

  disconnectedCallback() {
    this.shadowRoot.getElementById('dd-input').removeEventListener('click');
  }
}
window.customElements.define('drop-down', DropDown);
