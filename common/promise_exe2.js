function app() {
  return new Promise((resolve, reject) => {
    reject();
  });
}
const promise = app();
promise
  .then(() => {
    console.log('Promise Success 1');
  })
  .then(() => {
    console.log('Promise Success 2');
  })
  .then(() => {
    console.log('Promise Success 3');
  })
  .catch(() => {
    console.log('Promise Error 1');
  })
  .then(() => {
    console.log('Promise Success 4');
  });
