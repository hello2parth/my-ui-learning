function isPalindrome(string) {
  const halfLength = Math.floor(string.length / 2);
  for (let i = 0; i < halfLength; i++) {
    if (string.charAt(i) !== string.charAt(string.length - i - 1)) {
      return false;
    }
  }
  return true;
}

// using array slipt and reverse

function isPalindromeUsingSplit(string) {
  return string === string.split('').reverse().join('');
}
console.log(isPalindrome('racecar'));
console.log(isPalindromeUsingSplit('racecar'));
