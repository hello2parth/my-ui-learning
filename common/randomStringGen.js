function randomString(length) {
  let result = '';
  const alphabets = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const alphabetsLen = alphabets.length;
  for (let i = 0; i < length; i++) {
    result += alphabets.charAt(Math.floor(Math.random() * alphabetsLen));
  }
  return result;
}

console.log(randomString(10));
