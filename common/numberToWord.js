const dataset = {
  0: 'zero',
  ones: [
    'zero',
    'one',
    'two',
    'three',
    'four',
    'five',
    'six',
    'seven',
    'eight',
    'nine',
    'ten',
    'eleven',
    'twelve',
    'thirteen',
    'fourteen',
    'fifteen',
    'sixteen',
    'seventeen',
    'eighteen',
    'nineteen',
    'twenty',
  ],
  tens: ['', '', 'twenty', 'thirty', 'fourty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninty'],
  scale: ['', 'thousand', 'million', 'billion'],
};

function tripletToWord(triplet) {
  const result = [];
  if (triplet >= 100) {
    result.push(dataset.ones[Math.floor(triplet / 100)], 'hundred');
  }
  const remender = triplet % 100;
  if (remender) {
    if (remender > 0 && remender < 20) {
      result.push(dataset.ones[remender]);
    } else if (remender % 10 === 0) {
      result.push(dataset.tens[Math.floor(remender / 10)]);
    } else {
      result.push(dataset.tens[Math.floor(remender / 10)]);
      result.push(dataset.ones[Math.floor(remender % 10)]);
    }
  }
  return result.join(' ');
}

function numberToWord(num) {
  if (dataset[num]) {
    return dataset[num];
  }
  const sNum = String(num);
  const padding = 3 - (sNum.length % 3) === 3 ? 0 : 3 - (sNum.length % 3);
  const sNumWithPaddedZero = sNum.padStart(sNum.length + padding, '0');
  const aTriplet = [];
  for (let i = 0; i < sNumWithPaddedZero.length; i += 3) {
    aTriplet.push(sNumWithPaddedZero.substring(i, i + 3));
  }

  const result = [];
  let scaleIndex = 0;
  for (let i = 0; i < aTriplet.length; i++) {
    const triplet = aTriplet[i];
    const value = parseInt(triplet, 10);
    if (value > 0) {
      result.push(dataset.scale[scaleIndex], tripletToWord(value));
    }
    scaleIndex += 1;
  }
  return result.join(' ');
}

console.log(numberToWord(0));
console.log(numberToWord(99999));
