Array.prototype.listeners = {};

Array.prototype.addListener = function addListener(event, callBack) {
  if (!this.listeners[event]) {
    this.listeners[event] = [callBack];
  } else {
    this.listeners[event].push(callBack);
  }
};
