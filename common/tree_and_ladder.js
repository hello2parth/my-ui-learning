function printTree(row) {
  const column = 2 * row - 1;
  let result = '';
  for (let i = 0; i < row; i++) {
    let rowLine = ``;
    let noCharInRow = 2 * (i + 1) - 1;
    let noBlankSpaceEachSide = (column - noCharInRow) / 2;
    let blankSpace = ``;
    while (noBlankSpaceEachSide > 0) {
      blankSpace += ` `;
      noBlankSpaceEachSide--;
    }
    while (noCharInRow > 0) {
      rowLine += `*`;
      noCharInRow--;
    }
    result += `${blankSpace + rowLine + blankSpace}\n`;
  }
  console.log(result);
}

printTree(10);

function printLadder(steps) {
  let result = ``;
  for (let i = 0; i < steps; i++) {
    let rowLine = ``;
    let noOfChar = i + 1;
    while (noOfChar > 0) {
      rowLine += `*`;
      noOfChar--;
    }
    result += `${rowLine}\n`;
  }
  console.log(result);
}
printLadder(10);
