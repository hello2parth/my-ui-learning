/* eslint-disable no-bitwise */

/* Given an array which has all numbers repeating twice and just one number
present once. Find the non-repeating number in O(n) time complexity and
O(1) space complexity.
[1, 2, 3, 4, 5, 4, 3, 2, 1] */

// learn more about xor

function findNonRepeatingNo(array) {
  let num = null;
  array.forEach(v => {
    num ^= v;
    console.log({ v, num });
  });
  console.log(num);
}
const array = [1, 2, 3, 4, 5, 4, 3, 2, 1];
findNonRepeatingNo(array);
