const user = {
  name: 'parth',
  address: {
    city: 'Lunawada',
    state: 'Gujarat',
    address2: {
      city2: 'Lunawada',
      state2: 'Gujarat',
    },
  },
  surname: 'Patel',
  address3: {
    city1: 'Lunawada',
    state1: 'Gujarat',
    address4: {
      city4: 'Lunawada',
      state4: 'Gujarat',
    },
  },
};

function treeToFlat(source, result, append) {
  for (const key in source) {
    if (source[key]) {
      if (typeof source[key] === 'object') {
        treeToFlat(source[key], result, `${append}_${key}`);
      } else {
        result[`${append}_${key}`] = source[key];
      }
    }
  }
  return result;
}

console.log(treeToFlat(user, {}, 'user'));
