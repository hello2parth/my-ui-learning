function factorial(n) {
  if (n < 2) return 1;
  return n * factorial(n - 1);
}

console.log(factorial(5));

// call stack optimized solution

function factorialOpt(n) {
  const innerFun = function (n) {
    if (n < 2) {
      return 1;
    }

    return n * innerFun(n - 1);
  };
  return innerFun(n);
}
console.log(factorialOpt(5));

function factorialWRecurssion(n) {
  let result = 1;
  for (let i = 1; i <= n; i++) {
    result *= i;
  }
  return result;
}

console.log(factorialWRecurssion(5));
