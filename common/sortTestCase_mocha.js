const chai = require('chai');
const { expect } = chai;

const mergeSort = require('./mergeSort');
const bubbleSort = require('./bubbleSort');

const num = [2, 3, 4, 5, 6, 12, 345, 12, 1, 3, 345, 345, 12, 13, 12, 1, 1, 2, 3, 4, 2];

describe('merge sort test', () => {
  it('merge sort and normal sort result comparision', () => {
    expect(mergeSort(num)).to.deep.equal(bubbleSort(num));
  });
});
