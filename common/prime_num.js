function findPrimeNum(n) {
  const primeNum = new Array(n);
  primeNum.fill(true);
  primeNum[0] = false;
  primeNum[1] = false; // 1 and 2 are nethier prime nor non prime
  const squreRootLen = Math.floor(Math.sqrt(n));
  for (let i = 2; i < squreRootLen; i++) {
    for (let j = 2; j < n && i * j < n; j++) {
      primeNum[i * j] = false;
    }
  }
  const result = [];
  primeNum.forEach((v, i) => {
    if (v) {
      result.push(i);
    }
  });
  return result;
}

console.log(findPrimeNum(100));
