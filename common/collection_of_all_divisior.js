function getSqureRoot(x) {
  if (x < 2) return x;
  let start = 0;
  let end = x / 2;
  while (start <= end) {
    const mid = Math.floor((start + end) / 2);
    let res = x / mid;
    if (res === mid) {
      return mid;
    }

    if (res > mid) {
      start = mid + 1;
      res = mid;
    } else {
      end = mid - 1;
    }
  }
  return Math.floor(end);
}

function getDivisiors(num) {
  const aResult = [];
  const max = getSqureRoot(num);
  for (let i = 1; i <= max; i++) {
    if (num % i === 0) {
      if (i !== num / i && i * i !== num) {
        aResult.push(num / i);
      }
      aResult.push(i);
    }
  }
  return aResult;
}

function getRandomArbitrary(min, max) {
  return Math.floor(Math.random() * (max - min) + min);
}
const num = getRandomArbitrary(1, 100);

console.log(num, getDivisiors(num));

/* This problem has very simple solution, we all know that for any number ‘num’ all its divisors are always less than and equal to
‘num/2’ and all prime factors are always less than and equal to sqrt(num). So we iterate through ‘i’ till i<=sqrt(num) and for any 'i'
if it divides 'num' , then we get two divisors 'i' and 'num/i' , continuously add these divisors but for some numbers divisors 'i' and
'num/i' will same in this case just add only one divisor , e.g; num=36 so for i=6 we will get (num/i)=6 , that's why we will at 6 in the
summation only once. Finally we add one as one is divisor of all natural numbers. */
