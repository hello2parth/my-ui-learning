console.log.call(null, '1');

function job(state) {
    return new Promise(function(resolve, reject) {
		if (state) {
			resolve('Promise success');
		}
		else {
			reject('Promise error');
		}
	});
}
let promise = job(true);

promise
.then(function(data) {
    console.log(data);
    return job(false);
})

.catch(function(error) {
    console.log(error);
    return 'Promise Error caught';
})

.then(function(data) {
    console.log(data);
    return job(true);
})

.catch(function(error) {
    console.log(error);
});

console.log('2');

const demo = new Promise(resolve => {throw 'Hello World'});
demo.catch(() => 'Hello World 2').then(console.log.bind(console));

console.log('3')

function app() {
  return new Promise(function(resolve, reject) {
  reject();
  });
}
let promise1 = app();
promise1
.then(function() {
  console.log('Promise Success 1');
})
.then(function() {
  console.log('Promise Success 2');
})
.then(function() {
  console.log('Promise Success 3');
})
.catch(function() {
  console.log('Promise Error 1');
})
.then(function() {
  console.log('Promise Success 4');
});

console.log('4');

class DS{
  constructor(x){
    this.x = x;
  }
  static addDSTest(f, b){
    return f.x + b.x + b.y;
  }
}
class Test extends DS{
  constructor(x, y){
    super(x);
    this.y = y;
  }
}
let f = new DS(5);
let b = new Test(5, 5);
console.log(Test.addDSTest(f, b));

console.log('5');

function seq() {
  let i = 0;
  return function() {
    return i++;
  };
}
let seq1 = seq();
let seq2 = seq();

console.log( seq1() ); // line 1
console.log( seq1() ); // line 2
console.log( seq2() ); // line 3

console.log('6');
JSON.stringify( 1/0 );
